#ifndef SOCKIMPL_H
#define SOCKIMPL_H


#include <sock.h>
#include "testate.h"

class sockimpl
{
public:
    sockimpl(sock& socket):_rc(socket){}
    virtual ~sockimpl();
protected:
    eNUMEXP _receive(string& s, SADDR_46* p=0);
    eNUMEXP _send(const string& c,  SADDR_46* p=0);
    bool     _ereceive(uint8_t* buff, int exact);
    bool     _esend(const  unsigned  char* buff, int exact);
protected:
    sock& _rc;
};

#endif // SOCKIMPL_H
