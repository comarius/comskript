#ifndef OOHCLI_H
#define OOHCLI_H

#include <sock.h>
#include "remotetty.h"
#include "sockimpl.h"

class oohcli : public remotetty , public sockimpl
{
public:
    oohcli();
    virtual ~oohcli();

    eNUMEXP connect(const char* user, const char* pass, const char* host, int port, const string& args);
    eNUMEXP connect();
    eNUMEXP open_tty();
    eNUMEXP receive(string& s);
    eNUMEXP send(const string& c);
    eNUMEXP put_file(const char* src, const char* dst);
    eNUMEXP get_file(const char* src, const char* dst);
    bool     isopen();
    void     close();
protected:
    int _read_header(char* buff, int bytes, uint32_t& remlen);
private:
    tcp_cli_sock    _c;
    uint64_t       _session;
    int            _cycle;
};

#endif // OOHCLI_H
