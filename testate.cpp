

#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include "sqwrap.h"
#include "strutils.h"
#include "testate.h"
#include "nutconf.h"
#include "concontext.h"



const char* c_states[32]={
    "STOPPED",
    "STARTING",
    "LOGGINGIN",
    "RUNNING",
    "RUNNING",
    "OFFLINE",
    "ONLINE",
    "INPUT",
    "ERROR",
    "ERR_SCRIPT",
    "WARNING",
    "FLUSHING",
    "DEADLOCK",
    "PAUSED",
    "SUCCESS"
    "BREAK"
    "INTERACTIVE"
    ""
};


const char* c_exceptions[32]={
    "err_none",
    "err_connect",
    "err_network",
    "err_opentty",
    "err_auth",
    "err_timeout",
    "err_conbroken",
    "err_offline",
    "err_conresored",
    "err_resource",
    "err_ssh",
    "err_user",
    "err_pooling",
    "err_scriptsintax",
    "err_notsuporrted",
};

SQInteger sq_export_err(HSQUIRRELVM v)
{
    Sqrat::Enumeration error(v);
	error.Const("e_none", 0);
	error.Const("e_connect", 1);
	error.Const("e_network", 2);
	error.Const("e_opentty", 3);
	error.Const("e_auth", 4);
	error.Const("e_timeout", 5);
	error.Const("e_conbroken", 6);
	error.Const("e_offline", 7);
	error.Const("e_conresored", 8);
	error.Const("e_resource", 9);
	error.Const("e_ssh", 10);
	error.Const("e_user", 11);
	error.Const("e_pooling", 12);
	error.Const("e_script", 13);
	error.Const("e_notsupported", 14);

	Sqrat::ConstTable(v).Enum("error", error);
	Sqrat::Enumeration states(v);
	error.Const("s_stopped", 0);
	error.Const("s_starting", 1);
	error.Const("s_loggingin", 2);
	error.Const("s_inscript", 3);
	error.Const("s_running", 4);
	error.Const("s_offline", 5);
	error.Const("s_online", 6);
	error.Const("s_input", 7);
	error.Const("s_error", 8);
	error.Const("s_script_error", 9);
	error.Const("s_warning", 10);
	error.Const("s_flusing", 11);
	error.Const("s_hung", 12);
	error.Const("s_paused", 13);
	Sqrat::ConstTable(v).Enum("state", states);

	return 0;
}

const ctxerr& ctxerr::object()const
{
    return *this;
}


Progress::Progress(Context* pc, C_PROGRESS st,
            const stringstream& prog):_pc(pc)
{
    pc->log_prog(st, prog);
    std::ofstream ofs;
    ofs.open( pc->_fstack().c_str(), ios::out | ios::app  );
    if(ofs.is_open())
    {
        ofs << pc << ": " << prog.str() << "\n";
        ofs.close();
    }
    pc->_is_paused();
}

Progress::~Progress()
{
    _pc->_is_paused();
    if(_enabled)
    {
        string scr =  _pc->script();
        if(scr.empty()) scr="N/A";
        stringstream s; s<< scr;
        GLOGD("st: " << c_states[CP_INSCRIPT] <<": " << scr );
        _pc->log_prog(CP_INSCRIPT, s);
    }
}


void Progress::prog(C_PROGRESS st, const stringstream& prog)
{
    _pc->_is_paused();
    _pc->log_prog(st, prog);
}

