# comskript

### mMltithreaded scripted shell over serial/ssh/adb   
### Great to autmate testing of hundreds of computes on production line.
#### ooh protocol was designed specific for such usecase.   
#### custom raw text 'named: ooh' & file transfer client in tandem with 'ooh' server on remote.
   custom ooh supports side commands working to multiple targets at the same time.


Supports following protocols:

1. Telnet
2. SSH
  1. with sftp file transfer both directions
3. Android ADB over TCP/WIFI 
  1. with ADB file transfer over ADB
  2. Android shell (normal shell session)



### Command line:

    comskript script.file  protocol://<user/password>@device-location<:port> somename-id
        prototocol can be ssh, tty adb 
        device-location is the device ip or the device /dev/name
        somename-id  required for logging
        
        
####  Simple sample script

// play a song on R-PI, up to 50% of it's length.

Presume soong.scr is:
```javascript

function main(arg)
{
   term<=Term();
   term.login(arg);
   term.write("mplayer ./trooper.mp3","playing"); // write witch callback
}   

// called durring the play, every time mplayer yields a line
function playing(term, output)
{
    if(output.find("50"))
        return false;  // break and end the script
    return true;    
}

```
If R-PI IP is 192.168.23.23
then run on another machine on same network

```javascript
comskript song.scr  ssh://pi/raspberry@192.168.23.23:22 myrpi
```
 of tru a serial
 
```javascript
comskript song.scr  tty://pi/raspberry@/def/ttyUSB0 myrpi
```

 #### A multiline write

```javascript
function main(args)
{
    term <- Term();
    term.login("you can change the args here"); // 1. protocol:<username>/<password>@device_location:<port>
    term.mlwrite(@"
        cd ~
        ls -l
        cd /root
        ls -l
        cd ~
        echo asdf
    ",
    "proc");  /// -> calls proc
}


function proc(term, command, cmdout)
{
    print(command + "->" + cmdout + "\n");
}
```

####  To prompt for credentials replace user/pass with *

```javascript
function main(args)
{
    term <- Term();
    term.login("ssh://marius/*@127.0.0.1:22"); //prompts for passwords in cin because '*'
    term.show_term(args);
    print("            DONE");
    sleep(1);
    return 1;
}
```

#### run someting as sudo on remote machine

```javascript
function main(args)
{
    term <- Term();
    term.login("login:");
    term.mlwrite(@"
        cd ~
        ls -l
        cd /root
        ls -l
        cd ~
        echo asdf
    ",
    "proc");
    term.write("cd /etc");
    print(term.read());

    term.write("sudo ls","cbpass");  // cbpass() is called 'sudo ls' output
    term.expect();
    print(term.read());
    return 1;
}

//  shows mlwrite outout
function proc(term, ins, outs)
{
    print(ins + "->" + outs + "\n");
    return true; //keep it up, return 0 to end
}

function cbpass(term, output)
{
    switch(output)
    {
        case "[sudo] password for marius:":
            term.write("raspberry");
            return true;
       default:
            break;
    }
    print("|"+output+"|");
    return true;
}
```


#### A script which plays a song an a non configured raspberry pi

COnfigures r-pi, installs mplayer, configures jack, downloads song and play's it.

```javascript
function main(args)
{
    term <- Term();
    term.login("ssh://pi/pi@192.168.1.125:22");
    term.mlwrite(@"  // writes a script (multiline) on remote then execute it...
        sudo apt-get update
        sudo apt-get install -y mplayer
        [[ ! -f ./rtooper.mp3 ]] && get http://209.177.145.152/share/trooper.mp3
        sudo raspi-config
        #DOWN:7
        #ENTER
        #DOWN:8
        #ENTER
        #DOWN,
        #ENTER
        #RIGHT:2,
        #ENTER
        mplayer ./trooper.mp3
        ", 
        "playing_confirm");
    return 1;
}


function playing_confirm(t, output)
{
    if(output.find("11.5")!=null) //wait for a line that has 11.5
    {
                  t.write("pkill mplayer");
                  // when the tyest is in CP_INPUT state place the answer in cin file.
          // in this case 'y'
          local yesno = t.get_input("Did you hear the sound ? y/N. Write 'y' or 'n' in /logs/" + t.get_uid() + "/cin  file.");
          if(yesno.find("y")!=null)
          {
               t.report("sound test passed");
          }
          else
          {
               t.report("problems dude");
          }
          return false; // intrerrupt the write command and the song
    }

    return true; // keep playing
}

/*
  keyboard codes
  Left Arrow  37
  Up Arrow 38
  Right Arrow 39
  Down Arrow 40
  this function does:
# press Down arrow 7 times
# press enter
# press down 7 times
# press enter
# press down one time
# press enter
# press right 2 times
# press enter
*/
function configure_jack(term)
{
         if(term.wr("sudo raspi-config","Finish"))
     {
                local i;
                for(i=0;i<7;i++)
                        term.send_key("DOWN");
            term.send_key("ENTER");
        print(term.read());
                sleep(1);

        for(local i=0;i<8;i++)
                   term.send_key("DOWN");
                term.write("ENTER");
        print(term.read());
                sleep(1);

                term.send_key("DOWN");
                term.send_key("ENTER");
        print(term.read());
                sleep(1);

                term.send_key("RIGHT");
                term.send_key("RIGHT");
                print(term.read());
                term.send_key("ENTER");
        print(term.read());
                sleep(1);

            return term.expect();
     }
     return false;
}

```

Run on multiple machines

```javascript

//
// runs test on 98 computers
//
for(local l=2;l<100;l++)
{
    run("ssh://pi/pi@192.168.1." + l + ":22","test.fle");
}

```



#####  Scripts are for illustration purpose! See functional scripts in bin/tests folder. ? some links as http://...mp3 might be outaded ? 

### Free for non-profit organizations and sole users.

    Not for corporations. To be used by sole programmers, In house use only.
    Corporations, Ltd's Inc's SRL's, would need special written license from the author

Marius C. Oct 4 2017


