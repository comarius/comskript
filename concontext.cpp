


#include <iostream>
#include <stdlib.h>
#include <sys/termios.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sslcrypt.h>
#include <procinst.h>
#include <strutils.h>
#include <algorithm>

#include "main.h"
#include "fxngine.h"
#include "globalfoos.h"
#include "concontext.h"
#include "exterm.h"
#include "fileterm.h"
#include "testate.h"
#include "nutconf.h"
#include "ctxloger.h"

using namespace std;




//-----------------------------------------------------------------------------
int Context::_cindex = 0;

//-----------------------------------------------------------------------------
Context::Context():_iopump(this),_tout(PNCONF->_globo.cmdwait),_ttyonline(false),_exit_state(CP_STOPPED)
{
    ScriptData sd = PFX->get_data();
    _psqenv = sd._penv;
    _script = sd._script;
    _blog = PNCONF->_blog;
    Context::_cindex++;
    _init_logs(sd);
    GLOGI("new context:" << getpid() <<"-" <<this << ", "<< _script);

    PROG(CP_STARTING,"new term");

    ::fwriteline(_fthis(), std::to_string((uint64_t)this));
    ::chmod(_fthis().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
    ::fwriteline(_fpid(), std::to_string((uint64_t)getpid()));
    ::chmod(_fpid().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
    _outfifo = mkfifo(_fifofout().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);

    if(PNCONF->_globo.monitor)
        monitor();
}

//-----------------------------------------------------------------------------
Context::~Context()
{
    GLOGI("context deleted:" << getpid() <<"-" <<this << ", "<< _script);
    PROG(_exit_state, _exit_msg << " DONE");

    _PROGOFF();

    if(PFX)
    {
        ScriptData sd(_psqenv, _deviceid, _script, this);
        PFX->save_data(sd);
    }
    ::unlink(_fsystemp().c_str());
    if(_outfifo)::close(_outfifo);
}

//-----------------------------------------------------------------------------
// event frmo underlying thread when the remote goes away or is reconnected
void Context::tty_event(eNUMEXP code)
{
    GLOGI("Iothread tty_event" << (code+'A'));

    AutoLock    a(&_m);
    if(code == EX_CONBROKEN)
    {
         PROG(CP_OFFLINE, __FUNCTION__ << _OC(c_exceptions[code]));
        _ttyonline=false;
    }
    else if(code==EX_CONRESORED)
    {
        PROG(CP_ONLINE, c_exceptions[code]);
        _ttyonline=true;
    }
}

//----------------------------------------------------------------------------------------------------------
// reconnecting after reboot of offline, reusing same context
SCRIPT bool  Context::re_login()
{
    PROG(CP_LOGGINGIN, __FUNCTION__);

    ::fwriteline(_fuser(),"DEVICE OFFLINE");
    time_t fute = time(0) + PNCONF->_globo.onlinewait;
    while(intime(fute) && _ttyonline==false)
    {
        if(!_notify_script(__FUNCTION__, _credentials.c_str()))
            break;
        ::sleep(4);
    }
    if(_iopump.isopen())
    {
        _prompt=_iopump.get_prompt();

        GLOGI("host " << _ret << " prompt is: "<< _ret);
        GLOGI("re-logged in successfuly to: " << _host);
        _ttyonline = true;
        io_clear();

        fwriteline(_fcout(),_prompt);
        _PROG(CP_ONLINE, __FUNCTION__);
        return true;
    }
    GLOGE("cannot re_login to: " << _host);
    return false;
}

/*
    returns the device name passed in as third parameter of the flexte or the run()/spawn() last parameter
*/
const char*   Context::get_uid()
{
    PROG(CP_RUNNING, __FUNCTION__);
    return _deviceid.c_str();
}

/*
    returns the prompt string
*/
SCRIPT const char*   Context::login(const char* uph)
{
    _credentials = _str_login(uph);

    PROG(CP_LOGGINGIN, __FUNCTION__ << _OC(_credentials));

    eNUMEXP lasterr = EX_NONE;
    _parse_authstr(_credentials,_protocol,_user,_pass,_host,_port,_args);
    if(_pass=="*")
    {
        _PROG(CP_INPUT, "/cin file waits for password for: " << _user);
        fwriteline(_fuser(),"S:waiting for password");
        time_t fute = time(0) + PNCONF->_globo.inputwait;
        while(intime(fute))
        {
            if(::access(_fcin().c_str(),0)!=0)
            {
                ::sleep(1);
                continue;
            }
            string content = freadline(_fcin().c_str());
            if(content.length()>1)
            {
                _pass=content;
                fclear(_fcin());
                break;
            }
            ::sleep(1);
        }

    }
    time_t fute = time(0) + PNCONF->_globo.onlinewait;
    while(intime(fute))
    {
        try{
            _initialize(_protocol, _user, _pass, _host, _port, _args);
            break;
        }
        catch (ctxerr& err)
        {
            lasterr = err.code();
        }
        if(lasterr!= EX_CONNECT)
        {
            THROW(lasterr, __FUNCTION__);
        }
        if(!_notify_script(__FUNCTION__, c_exceptions[EX_CONNECT]))
        {
            break;
        }
        ::sleep(4);
    }
    if(!_iopump.isopen() || lasterr != EX_NONE)
    {
        THROW(lasterr, __FUNCTION__);
    }
    _ret=_prompt;
    _iopump.start_thread();
    ::sleep(1);
    _iopump.clear();
    io_clear();

    ::fwriteline(_fuser(),_user);
    ::fwriteline(_fhost(),_host);
    ::fwriteline(_fproto(),_protocol);
    return _ret.c_str();
}

/*
    is the remote system has no outstanding unread output
*/
bool  Context::is_ready()
{
    PROG(CP_RUNNING, __FUNCTION__);

    if( fflength(_fcout())==0 )return 1;
    if(!_fromshell.empty()) return 0;

    string fnd = ::fsearch(_fcout(),_prompt);
    if(!fnd.empty())
        return 1;
    return 0;
}


/*
    writes and on respons calls the cllabsk of callback is present
    keep calling callback as long the remote host has output in
    times frames less than to timout
*/
bool Context::write(const char* cmd, const char* cb, int to)
{
    string      s;
    int         bytes = 1;

    if(cmd==0 || *cmd==0)
        return false;
    PROG(CP_RUNNING, __FUNCTION__<< _O(cmd) << _P(_U(cb)) << _C(to));
    _ret.clear();
    if(!is_ready())
    {
        _PROG(CP_WARNING, __FUNCTION__ <<  ": not ready");
        GLOGW(__FUNCTION__ << " while output in 'cout'. flushing to backup" );
        _backup(_fcout());
        this->io_flush();
        this->io_clear();
       _notify_script("on_warning", "backup");
    }
    _fromshell.clear();
    s = cmd;
    if(s.find('\r')==string::npos)  s.append("\r");
    if(!_to_shell(s))
    {
        THROW(EX_CONBROKEN, cmd);
    }
    if(cb && *cb!=0)
        bytes = _keep_reading(to, cb);

    return bytes>0;
}



/*
    writes and expects exp while timouts
*/
const char*  Context::wr_rd(const char* cmd, int to)
{
    string      s;

    PROG(CP_RUNNING, __FUNCTION__ << _OC(cmd) << _C(to));
    if(to==0)to=PNCONF->_globo.cmdtime;
    _ret.clear();
    if(!is_ready())
    {
        _PROG(CP_WARNING, __FUNCTION__ <<  ": not ready");
        GLOGW(__FUNCTION__ << " while output in 'cout'. flushing to backup" );

        _backup(_fcout());
        this->io_flush();
        this->io_clear();
        _notify_script("on_warning", "backup");
    }
    s = cmd;
    if(s.find('\r')==string::npos)
        s.append("\r");
    _fromshell.clear();
    if(!_to_shell(s))
    {
        THROW(EX_CONBROKEN, cmd);
    }
    _keep_reading(to,0);
    _ret = _fromshell;
    _fromshell.clear();
    return _ret.c_str();
}


/*
    writes and expects exp while timouts
*/
bool  Context::wr_ex(const char* cmd, const char* exp, int to)
{
    string      s;

    PROG(CP_RUNNING, __FUNCTION__ << _OC(cmd) << _P(exp) << _C(to));
    if(to==0)to=PNCONF->_globo.cmdtime;
    _ret.clear();
    if(!is_ready())
    {
        _PROG(CP_WARNING, __FUNCTION__ <<  ": not ready");
        GLOGW(__FUNCTION__ << " while output in 'cout'. flushing to backup" );

        _backup(_fcout());
        this->io_flush();
        this->io_clear();
        _notify_script("on_warning", "backup");
    }
    s = cmd;
    if(s.find('\r')==string::npos)
        s.append("\r");
    _fromshell.clear();
    if(!_to_shell(s))
    {
        THROW(EX_CONBROKEN, cmd);
    }
    if(exp==0 || (exp && *exp==0))
    {
        return expect(_prompt.c_str() ,"",to);
    }
    return expect(exp ,"",to);
}

/*
    put's a staement in the progress file
*/
int Context::log_status(int status, const char* log)
{
    if(status <= (int)CP_DEADLOCK)
    {
        PROG((C_PROGRESS)status, log);
        _PROGOFF();
    }
    else
    {
        GLOGW("cannot log statuses greate than 15. Did yuo read the spec?");
    }
    return 1;
}

/*
    reads or accumulatates the output from remote device
    as long there is output comming in in less than to
    intervals.
*/
const char* Context::read(const char* cb, int to)
{
    if(to==0)
        to=_tout;
    PROG(CP_FLUSHING, __FUNCTION__ << _OC(_U(cb))<<_OC(to));
    _keep_reading(to, 0);       // put in cout
    _dump_file(_fcout(), cb);
    _ret = _fromshell;
    _fromshell.clear();
    return _ret.c_str();
}

//-----------------------------------------------------------------------------
SCRIPT bool Context::reading()
{
    PROG(CP_FLUSHING, __FUNCTION__);
    if(fflength(_fbackup()))
    {
        _dump_file(_fbackup(), __FUNCTION__);
    }
    if(fflength(_fcout()))
    {
        _dump_file(_fcout(), __FUNCTION__);
    }
    fclear(_fbackup());
    fclear(_fcout());
    return true;
}

//-----------------------------------------------------------------------------
// no console output for
bool  Context::is_steady(int t)
{
    PROG(CP_FLUSHING, __FUNCTION__);

    string  s;
    if(t==0) t=PNCONF->_globo.cmdtime;
    time_t fute = time(0) + t;
    while(intime(fute))
    {
        if(_iopump.from_shell(s))
        {
            fute = time(0) + t;
        }
        ::msleep(200);
    }
    return true;
}

//-----------------------------------------------------------------------------
int  Context::logout()
{
    PROG(CP_RUNNING, __FUNCTION__);

    string s = "exit\r\n";
    if(!_to_shell(s))
    {
        THROW(EX_CONBROKEN, "logout");
    }
    return 1;
}

//-----------------------------------------------------------------------------
bool  Context::io_flush(int to)
{
    string s;

    PROG(CP_FLUSHING, __FUNCTION__);
    _iopump.flush(_tout);
    time_t fute = time(0) + to ? to : _tout;
    while(_from_shell(s) && intime(fute))
    {
        fute = time(0) +  to ? to : _tout;
        if(!_notify_script(__FUNCTION__, s.c_str()))
            break;
        ::usleep(1000);
    }
    return true;
}


string Context::_str_login(const char* uph)
{
    string loco;

    if(uph && uph[0] && uph[0]=='@')
    {
        loco = uph+1;
    }
    else
    {
        if(!PFX->credentials().empty())
        {
            loco=PFX->credentials().c_str();
        }
        else
        {
            if(uph && *uph)
                loco = uph;
            else
                loco = _credentials;
        }
    }
    return loco;
}

//-----------------------------------------------------------------------------
bool Context::send_key(const char* code)
{
    struct Skc
    {
       char name[8];
       struct Code{
            uint8_t a;
            uint8_t b;
            uint8_t c;
            uint8_t d;
       }chrs;
    }  kmap[] = {

    {"^C",{0x3,0x0,0x0,0x0}},
    {"ESC",{0x1B,0x0,0x0,0x0}},
    {"F1",{0x1B,0x4F,0x50,0x0}},
    {"UP",{0x1B,0x5B,0x41,0x0}},
    {"DOWN",{0x1B,0x5B,0x42,0x0}},
    {"LEFT",{0x1B,0x5B,0x44,0x0}},
    {"RIGHT",{0x1B,0x5B,0x43,0x0}},
    {"DEL",{0x1B,0x5B,0x33,0x7E}},
    {"INS",{0x1B,0x5B,0x32,0x7E}},
    {"END",{0x1B,0x4F,0x46,0x0}},
    {"HOME",{0x1B,0x4F,0x48,0x0}},
    {"ENTER",{0x0D,0x00,0x0,0x0}},
    {"^M",{0x0D,0x00,0x0,0x0}},
    {"",{0x1B,0x5B,0x45,0x0}},
    };

    union U
    {
        Skc::Code ck;
        char      codes[4];
    };
    char loco[16];

    ::strcpy(loco, code);
    int   times = 1;
    char* ps = ::strchr(loco,':');
    if(ps) {*ps=0; times = ::atoi(ps+1);}
    Skc* psk = 0;//&kmap[0];
    for(size_t k=0;k<sizeof(kmap)/sizeof(Skc);++k)
    {
        if(!::strcmp(loco,kmap[k].name))
        {
            psk = &kmap[k];
            break;
        }
    }

    if(psk)
    {
        U    pu;  pu.ck=psk->chrs;

        PROG(CP_RUNNING, __FUNCTION__ << _O(loco)<<_C(times));
        if(!is_ready())
        {
            _PROG(CP_WARNING, __FUNCTION__ <<  ": not ready");
            GLOGW(__FUNCTION__ << " while output in 'cout'. flushing to backup" );
            _backup(_fcout());
            this->io_flush();
            this->io_clear();
           _notify_script("on_warning", "backup");
        }


        string s = (const char*)(pu.codes);

        for(int i=0;i<times;i++)
        {
            ::fwriteline(_fcout(), code);
            _fromshell.clear();
            if(!_to_shell(s))
            {
                THROW(EX_CONBROKEN,code);
            }
            ::msleep(256);
        }
        return true;
    }
    return false;
}

//-----------------------------------------------------------------------------
int Context::sleep(int t)
{
    PROG(CP_RUNNING, __FUNCTION__<<_OC(t));

    if(t < 600) ///10 minutes
    {
        time_t fute = time(0) + t;

        for(int i=t; i>0 && intime(fute);--i)
        {
            //if(!_notify_script(__FUNCTION__, std::to_string(i).c_str()))
            //    break;
            ::sleep(1);
        }
    }
    return 1;
}


//-----------------------------------------------------------------------------
bool Context::show_term(const char* uph)
{
    string cred =  _str_login(uph);

    PROG(CP_RUNNING, __FUNCTION__ << _OC(cred));

    string pr,ho,us,pa,ar;
    size_t port;
    //const string& uph, string& proto, string& user,string& pass,string& host,int& port,string& args
    _parse_authstr(cred,pr,us,pa,ho,port,ar);
    if(PNCONF->_globo.terminal=="fileterm")
    {
        fileterm ex;
        ex.iotty(pr,us,pa,ho,port,ar,_deviceid);
    }
    else
    {
        exterm ex;
        ex.iotty(pr,us,pa,ho,port,ar,_deviceid);
    }
    return true;
}

int Context::report(const char* s)
{
    PROG(CP_RUNNING, __FUNCTION__ << _OC(s));

    Color::Modifier red(Color::FG_RED);
    Color::Modifier def(Color::FG_DEFAULT);

//    std::cout <<red<< "===========================================================================\n";
//    std::cout << "reporting: " << _deviceid << ": \n" << s << "\n";
//    std::cout << "===========================================================================" << def <<"\n";

    string repl = (s);
    undecorate(repl);
    ::fwriteline(_freport(), repl, true);
    return 1;
}

//-----------------------------------------------------------------------------
bool Context::expect(const char* what, const char* cb, int to)
{
    bool    b;
    string s;

    PROG(CP_RUNNING, __FUNCTION__ << _O(what) << _P(_U(cb)) << _C(to));
    if(to>1000||to==0)
    {
        to=_tout;
    }
    _fromshell.empty();
    if(to==0)
        to=PNCONF->_globo.cmdwait;
    time_t fute = time(0) + to;

    _PROG(CP_FLUSHING, __FUNCTION__ << _OC(what));
    while(intime(fute))
    {
        s.clear();
        if(_from_shell(s))
            fute = time(0) + to;

        if(what[0]=='%') //use regular expression
        {
            _fromshell = ::fsearchreg(_fcout(), &what[1]);
        }
        else
        {
            _fromshell = ::fsearch(_fcout(), what);
        }

        if(_fromshell.length()>0)
        {
            GLOGD("expect("<< what << ") -> FOUND in cout.");
            _fromshell = what;
            break;
        }
        //if we got the prompt output is done
/*
        if(_prompt!=what)
        {
            _fromshell = ::fsearch(_fcout(), _prompt);
            if(!_fromshell.empty())
            {
                GLOGD("expect("<< _prompt << ") -> FOUNDin cout. abandoning.");
                _PROG(CP_WARNING, __FUNCTION__ << _OC("found prompt INSTEAD"));
                //_fromshell.clear();
                ::sleep(1);
                break;
            }
        }
*/

        if(cb && *cb)
        {
            if(!_notify_script(cb, what))
                break;
        }
    }
    if(_fromshell.empty())
    {
        CLOGD(what << " was NOT found");
    }
    return _fromshell.empty() ? 0 : 1;
}

const char*    Context::pick()
{
    PROG(CP_RUNNING, __FUNCTION__);
    _ret=_fromshell;
    _fromshell.clear();
    return _ret.c_str();
}

const char* Context::get_input(const char* prompt)
{
    PROG(CP_RUNNING, __FUNCTION__<< _OC(prompt));


    ::fwriteline(_fuser(), string(prompt));
    ::fclear(_fcin());
    _fromshell.clear();
    time_t now = time(0);
    time_t fute = now + PNCONF->_globo.inputwait;
    struct stat st;
    do{
        stat(_fcin().c_str(), &st);
        ::sleep(3);
        if(!_notify_script(__FUNCTION__, prompt))
        {
            break;
        }
        std::cout << "***** cin waits for input ***** \n" << freadline(_fuser()) << "***********\r";
    }while(st.st_size==0 && intime(fute));
    ::msleep(100);
    _fromshell = ::freadline(_fcin());
    fclear(_fcin());
//    fclear(_fuser());
    if(!_fromshell.empty())
    {
        CLOGD("input received: " << _fromshell);
    }
    else
    {
        THROW(EX_TIMEOUT, prompt);
    }
    _ret = _fromshell;
    return _ret.c_str();
}

const char* Context::mlwrite(const char* mline, const char* cb, int to)
{
    PROG(CP_RUNNING, __FUNCTION__<< _O(mline) << _C(cb));

    stringstream    s(mline);
    string          line;

    if(!is_ready())
    {
        _PROG(CP_WARNING, __FUNCTION__ <<  ": not ready");
        GLOGW(__FUNCTION__ << " while output in 'cout'. flushing to backup" );
        _backup(_fcout());
        this->io_flush();
        this->io_clear();
       _notify_script("on_warning", "backup");
    }
   _fromshell.clear();

    while(getlinecrlf(s,line))
    {
        if(line[0]=='#')
        {
            send_key(line.c_str());
            continue;
        }

        line.append("\r");
        if(_iopump.to_shell(line))
        {
            string s;
            ::msleep(200);
            time_t fute = time(0) + to == 0 ? PNCONF->_globo.outlength : to;
            while(intime(fute))
            {
                if(_iopump.from_shell(s))
                {
                    if(_fromshell.length()<(size_t)PNCONF->_globo.outlength)
                    {
                        _fromshell.append(s);
                    }
                    else
                    {
                        GLOGW("Output truncated for:" << mline << " appending to backup");
                        ::fwriteline(_fbackup(),s,true);
                    }
                    fute = time(0) + to == 0 ? PNCONF->_globo.outlength : to;
                }
                ::msleep(200);
            }
            //nootify line by line
            if(cb && *cb)
            {
                string           oline;
                stringstream    so(_fromshell);
                while(getlinecrlf(so,oline))
                {
                    if(!_notify_script(cb, line.c_str(), oline.c_str()))
                    {
                        break;
                    }
                }
            }
            _fromshell.clear();
        }
    }
    _ret=_fromshell;
    _fromshell.clear();
    return _ret.c_str();
}



//we notify only in pause because can create circular calls
int Context::pause(bool pause)
{
    PROG(CP_RUNNING, __FUNCTION__<< _OC(pause));

    if(pause)
    {
        ::fwriteline(_fpause(),"paused");
        ::chmod(_fpause().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
    }
    else
    {
        ::unlink(_fpause().c_str());
    }
    return pause;
}

void Context::_is_paused()
{
    if(::access(_fbreak().c_str(),0)==0)
    {
        PROG(CP_BREAK, __FUNCTION__);
        THROW(EX_USER, "User break/stop");
    }
    if(::access(_fpause().c_str(),0)==0)
    {
        PROG(CP_PAUSED, _script);
        _PROGOFF();
        int seconds = 1;
        while(::access(_fpause().c_str(),0)==0 &&  Inst.alive())
        {
            string secs = std::to_string(seconds);
            if(!_notify_script("pause", secs.c_str()))
                break;
            ::sleep(4);
        }
        _PROG(CP_RUNNING, __FUNCTION__);
    }

    if(::access(_finter().c_str(),0)==0)
    {
        PROG(CP_INTER, _script);
        _PROGOFF();
        show_term(::freadline(_finter()).c_str());
        ::unlink(_finter().c_str());
        _PROG(CP_RUNNING, __FUNCTION__);
    }
}

//-----------------------------------------------------------------------------
const char* Context::get_prompt()
{
    PROG(CP_RUNNING, __FUNCTION__);
    _ret = _prompt;
    return _ret.c_str();
}

//-----------------------------------------------------------------------------
bool Context::_notify_script(const char* funcname, const char* line, const char* lx)
{

    bool     rv  = true;
    string fname = funcname;


    //GLOGD(__FUNCTION__ << " " <<funcname<<"("<<line<<")");
    Sqrat::Function func = Sqrat::RootTable().GetFunction(_SC(fname.c_str()));
    if(!func.IsNull())
    {
        PROG(CP_INSCRIPT,  fname << _OC(line));
        try{

            if(lx)
                rv = func.Evaluate<bool>(this, line, lx);
            else
                rv = func.Evaluate<bool>(this, line);
            if(rv==false)
            {
                GLOGT(fname << "("<<line<<")  breaking callback. breaking callback. breaking callback.");
            }
            if(_prompt==line)
            {
                GLOGT(fname << "("<<line<<")  breaking callback. found prompt.");
                rv=false;
            }

        }catch(Sqrat::Exception ex)
        {
            THROW(EX_SCRIPT, ex.Message());
        }
    }
    else
    {
        //GLOGX(fname << "("<<line<<")  script missing.");
    }
    if(PFX->wasq_error())
        THROW(EX_SCRIPT, PFX->ssq_error());
    ::msleep(10);
    return rv;
}

int Context::exito(const char* message)
{
    PROG(CP_RUNNING, __FUNCTION__ << _OC(message));
    char esc[4] = {0};
    esc[0]=0x3;
    _to_shell(string(esc));
    ::msleep(1000);
    _to_shell(string("exit\r"));
    ::msleep(1000);
    THROW(EX_USER,message);
    return 1;
}


//-----------------------------------------------------------------------------
// sets new timout to iopump
int  Context::set_timout(int secs)
{
    PROG(CP_RUNNING, __FUNCTION__ << _OC(secs));
    _tout = secs;
    _iopump.set_timout(_tout);
    return _tout;
}

//-----------------------------------------------------------------------------
int Context::io_clear()
{
    PROG(CP_RUNNING, __FUNCTION__);
    ::fclear(_fcin());
    ::fclear(_fcout());
    return 1;
}

//-----------------------------------------------------------------------------
bool Context::put_file(const char* local, const char* remote)
{
    PROG(CP_RUNNING, __FUNCTION__ << _O(local) << _C(remote));

    stringstream s; s << local << "->" << remote;
    eNUMEXP ret = _iopump.put_file(local,remote);
    if(ret == EX_NONE)
    {
        return true;
    }
    return false ; //not reached

}

//-----------------------------------------------------------------------------
bool Context::get_file(const char* remote, const char* local)
{
    PROG(CP_RUNNING, __FUNCTION__ << _O(local) << _C(remote));

    stringstream s; s << local << "<-" << remote;
    eNUMEXP ret = _iopump.get_file(remote,local);
    if(ret ==EX_NONE)
    {
        return true;
    }
    return false; //not reached
}

bool   Context::download(const char* remote)
{
    PROG(CP_RUNNING, __FUNCTION__ << _OC(remote));

    string file = basename(remote);
    string here=_logpath; here+="/"; here+=file;
    int ir =  get_file(remote, here.c_str());
    return ir!=0;
}

bool Context::monitor()
{
    if(_mont.sock()<=0)
    {
        string  mon="/tmp/mon";
        mon += _deviceid;
        string local = mon;
        string rem  = mon;

        local+="_S";
        rem+="_C";

        if(_mont.create(local.c_str(), rem.c_str()))
        {
            char io[128];

            string exec="./ftcio ";
            exec += "\"";
            exec += mon;
            exec += "\" ";
            exec +="&";

            ::sprintf(io,PNCONF->_globo.terminal.c_str(), exec.c_str());
            GLOGD(io);
            ::system(io);
        }
    }
    return _mont.sock()>0;
}

int  Context::send_passwd()
{
    PROG(CP_RUNNING, __FUNCTION__);
    string p = _pass + "\r";
    _iopump.to_shell(p);
    return 1;
}

void Context::_mon(const char* dir, const string& data)
{
    if(_mont.sock())
    {
        _mont.send(dir, strlen(dir));
        _mont.send(data.c_str(), data.length());
        _mont.send("\r\n",2);
    }
}


/**
 keep accumulating and flush on notificationw hwnw there is no output for 500ms
*/
int Context::_keep_reading(int to, const char* cb)
{
    int     faketime = 0;
    bool    noty=false;
    size_t  outlen = 0;
    int     totlen=0;
    bool     retval=true;
    string  s;

    PROG(CP_FLUSHING, __FUNCTION__ << " CB->" << _U(cb));
    _fromshell.clear();
    ::fclear(_fcout());
    if(to==0)to=PNCONF->_globo.cmdwait;
    to   += 2;
    ::msleep(200);
    time_t fute = time(0) + to + 1;
    while(intime(fute))
    {
        s.clear();
        if(_from_shell(s))
        {
            noty = (s.find('\r')!=string::npos) || (s.find('\n')!=string::npos);
            faketime=0;           //reset fake time counter
            outlen += s.length();
            totlen += s.length();
            _fromshell.append(s);
            fute = time(0) + to+1;   // enlarge the tout because we got some output
        }

        if(cb&&*cb)
        {
            // notify script callback if there are LF CR or time > 500ms
            if(faketime>5 || noty || outlen > (size_t)PNCONF->_globo.outlength)
            {
                string line;
                strremove_377(_fromshell);
                undecorate(_fromshell);

                stringstream ss(_fromshell);

                while( ::getlinecrlf( ss, line ) ) {
                    strimrl(line);
                    if(line.empty())continue;
                    line+="\n";
                    if(!(retval=_notify_script(cb, line.c_str())))
                    {
                        break;
                    }
                    fute = time(0) + to+1;   // enlarge the tout because we got some output
                }
                _fromshell.clear();
                faketime=0;
                outlen=0;
                noty=false;
                if(retval==false)
                    break;
            }
        }
        ::msleep(100);
        ++faketime;
    }
    return totlen;
}


// lets make local tty raw

string kbhit(void)
{
    char kb[100];
    struct timeval tv = { 0L, 1000L };
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(0, &fds);
    if(select(1, &fds, NULL, NULL, &tv))
    {
        int nbytes = read(0, kb, sizeof(kb));
        if(nbytes)
        {
            kb[nbytes] = 0;
            return string(kb);
        }
    }
    return string("");
}

void Context::_initialize(const string& proto, const string& user, const string& pass, const string& host, int port, const string& args)
{
    _iopump.stop_thread();
    _iopump.reinit();
    eNUMEXP err = _iopump.connect_tty(proto, user, pass, host, port, args);
    if(err!=EX_NONE)
    {
        THROW(err, "initialize remote");
    }
    _prompt = _iopump.get_prompt();
    GLOGD("host " << _host << " prompt is: "<< _prompt);
    _ttyonline = true;
}

void Context::_tty_run()
{
    string iostring;
    string cmdline;

    while( _iopump.isopen() )
    {
        if(_from_shell(iostring))
        {
            cout << iostring;
            cout.flush();
        }
        string shot = kbhit();
        if(!shot.empty())
        {
            if(shot!="\n" && shot!="\r" && shot!="\r\n")
                cout << shot;
            cmdline.append(shot);
            if(cmdline.find('\r')>=0 || cmdline.find('\n')>=0)
            {
                _to_shell(cmdline);
                cmdline.clear();
            }

        }
        cout.flush();
        usleep(0xFFF);
    }
}




void Context::_parse_authstr(const string& uph,
                            string& proto,
                            string& user,
                            string& pass,
                            string& host,
                            size_t& port,
                            string& args)
{
    /*
        ssh://user/pass@host:port   -> _user,_pass,_host,_port
        ooh://host:ip               -> _host,_port
        tty:///dev/name:9600,n,8,1  -> dev/name:s,8,1->_host
        udp://iface:port            -> _host
        adw://2.2.2.2:SER_PORT:DEVPORT
        adb://SERIAL:SER_PORT
    */
    port=0;
    host.clear();
    proto = uph.substr(0,6);
    if( proto!="ssh://" &&
        proto!="adw://" &&
        proto!="ooh://" &&
        proto!="tty://" &&
        proto!="udp://" &&
        proto!="adb://")
    {
        GLOGE("cannot understant connection string: " << _protocol);
        throw(ctxerr(_logpath, EX_NOTSUPORTED, this));
    }
    string leftover = uph.substr(6);
    if(leftover[0]=='/') //special syntax
    {
        host = leftover;
        user = "(serial)";
        port = 0;
        return;
    }

    string  s = leftover;
    string  a;

    for(size_t ch = 0; s[ch]!=0; ch++)
    {
        if(s[ch]=='/')
        {
            user=a;
            a.clear();
            continue;
        }
        if(s[ch]=='@')
        {
            pass=a;
            a.clear();
            continue;
        }
        if(s[ch]==':')
        {
            if(host.empty())
            {
                host=a;
                a.clear();
                continue;
            }

            if(port==0)
            {
                port = ::atoi(a.c_str());
                a.clear();
                continue;
            }

        }
        a+=s[ch];
    }
    if(_port==0 && !a.empty())
        port = ::atoi(a.c_str());
    else
        args=a;
   if(port==size_t(-1))
    port=0;
}

//-----------------------------------------------------------------------------
void  Context::log_prog(C_PROGRESS state, const stringstream& ctnt)
{
    string step = ctnt.str();

    fxngine::chekin();

    std::replace( step.begin(), step.end(), '\r', ' ');
    std::replace( step.begin(), step.end(), '\n', ' ');
    while(step.length()<50)step.append(" ");

    std::ofstream ofs;
        ofs.open( _fprogress(), ios::out | ios::app );
        ofs << str_time()<<" & "<< uint64_t(this) <<"\t&\t"<< step <<"\t&\t"<<  c_states[state] << "\n";
    ofs.close();

    ofs.open( _fstatus(), ios::out  | std::ofstream::trunc );
        ofs << str_time()<<" & "<< uint64_t(this) <<" \t&\t"<< step <<"\t&\t"<<  c_states[state] << "\n";
    ofs.close();
}

void  Context::_init_logs(const ScriptData& sd)
{
    _deviceid = sd._deviceid;
    _logpath = PNCONF->_globo.testbase;
    _logpath += "/";
    _logpath += _deviceid;

    if(PNCONF->_globo.distinct)
    {
        struct timeval unique;
        ::usleep(1000);
        gettimeofday(&unique, NULL);
        _logpath += "_";
        _logpath += std::to_string(Context::_cindex);
    }
    _logpath += "/";

    stringstream s;
    s << _logpath << "\n";
    ::fwriteline(_fsystemp(), _deviceid+"\n");
    ::fwriteline(_fsystemp(), s.str(), true);
    ::chmod(_fsystemp().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
    string syscmd = "mkdir -p ";
    syscmd += _logpath;
    ::system(syscmd.c_str());
    ::chmod(_logpath.c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);

    ::fclear(_fcin());
    ::fclear(_fcout());
    ::fclear(_fstatus());
    ::fclear(_fprogress());
    ::fclear(_fstack());
    ::fclear(_freport());
    ::fclear(_finout());
    ::fwriteline(_ftemp(), _script);
    ::fwriteline(_fuser(), _user);
    ::fwriteline(_fuid(), _deviceid);
    ::fwriteline(_fuip(), _host);
    ::fwriteline(_fscript(), _script);
    ::unlink(_fbreak().c_str());

    ::chmod(_fcin().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
    ::chmod(_fcout().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
    ::chmod(_fstatus().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
    ::chmod(_fprogress().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
    ::chmod(_fstack().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
    ::chmod(_finout().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
    ::chmod(_ftemp().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
    ::chmod(_fuser().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
    ::chmod(_fuid().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
    ::chmod(_fuip().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
    ::chmod(_fscript().c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
}

bool Context::intime(time_t future)
{
    if(PFX->wasq_error())
        THROW(EX_SCRIPT, PFX->ssq_error());
    fxngine::chekin();
    if (::access(_fbreak().c_str(),0)==0)
    {
        GLOGT("intime expired due break");
        return false;
    }
    if(time(0) > future)
    {
        GLOGT("intime expired due fut:"<< time(0)-future);
        return false;
    }
    return Inst.alive();
}


bool  Context::_flushoutput(const char* caller, int to,bool noty)
{
    string s;
    time_t fute  = time(0) + to ? to : _tout;
    while(intime(fute))
    {
        s.clear();
        if(_from_shell(s))
        {
            fute = time(0) + to ? to : _tout;
            if(noty)
            {
                if(!_notify_script(caller, s.c_str()))
                    break;
            }
        }
        ::msleep(200);
    }
    return fflength(_fcout()) > 0;
}

bool  Context::_store_output(const string& o)
{
    return false;
}

bool Context::_read_output(string& ret)
{
    return true;
}




bool  Context::_to_shell(const string& s)
{
    ::fwriteline(_fcin(), s);
    ::fwriteline2(_finout(),">", s, true);
    GLOGD("ctx:>>" << s);
    _mon("> ", s);
    _lastcmd_2_shell = s;

    ::sstr_replace(_lastcmd_2_shell,'\r',' ');
    ::sstr_replace(_lastcmd_2_shell,'\n',' ');
    ::strimrl(_lastcmd_2_shell);

    return _iopump.to_shell(s);
}

bool  Context::_from_shell(string& s)
{
    bool    b = _iopump.from_shell(s);
    if(false==b)  return b;
    ::strremove_377(s);
    if(!_lastcmd_2_shell.empty())
    {
        string loco = s;
        if(loco.find("\n") != string::npos ||
           loco.find("\r") != string::npos)
        {
            stringstream sss;
            string       line;

            sss << loco;
            while(getlinecrlf (sss, line))
            {
                if(line.empty())continue;
                undecorate(line);
                ::strimrl(line);
                if(line.empty())continue;
                if(!line.empty() && line == _lastcmd_2_shell)
                {
                    GLOGW("term-echo: " << _lastcmd_2_shell);
                    _lastcmd_2_shell.clear();
                }
                else
                {
                    GLOGD("cout<"<<line);
                    ::fwriteline(_fcout(), line, true);
                    ::fwriteline2(_finout(),"<", line, true);
                }
            }
        }
    }
    else
    {
        GLOGD("cout<"<<s);
        ::fwriteline(_fcout(), s, true);
        ::fwriteline2(_finout(),"<", s, true);
    }
    _mon("< ", s);
    return b;
}



void Context::_backup(const string & file)
{
    stringstream cmd;

    cmd << "echo " << "# " << file <<" content at: " << str_time() << ";";
    cmd << "cat " << file << " >> " << _fbackup() << ";\n";
    system(cmd.str().c_str());
}

bool Context::_dump_file(const string& sfile, const char* cb)
{
    std::ifstream file (sfile.c_str());

    _fromshell.clear();
    if(file.is_open())
    {
        std::string line;
        while(getlinecrlf(file, line))
        {
            fxngine::chekin();
            if(cb && *cb)
            {
                strimrl(line);
                if(line.empty()) continue;
                if(!_notify_script(cb, line.c_str()))
                    break;
            }
            else
            {
                if(_fromshell.length()<(size_t)PNCONF->_globo.outlength)
                {
                    if(line!="\n")
                    {
                        _fromshell.append(line);
                        _fromshell.append("\n");
                    }
                }
                else
                {
                    GLOGW("Truncated output (more than outlength): out was backuped in backup file" << line);
                    if(sfile != _fbackup())
                        _backup(sfile);
                    ::fclear(sfile);
                    break;
                }
            }
        }
        file.close();
    }
    ::fclear(sfile);
    return !_fromshell.empty();
}

void     Context::sq_export(SqEnv& env)
{
    GLOGD("exporting class methods to script environment");
    Sqrat::Class<Context> myClass(env.theVM());

    myClass.Overload<bool (Context::*)(const char* )>(_SC("wr_ex"), &Context::wr_ex);
    myClass.Overload<bool (Context::*)(const char*, const char*)>(_SC("wr_ex"), &Context::wr_ex);
    myClass.Overload<bool (Context::*)(const char*, const char*, int)>(_SC("wr_ex"), &Context::wr_ex);

    myClass.Overload<const char* (Context::*)(const char* )>(_SC("wr_rd"), &Context::wr_rd);
    myClass.Overload<const char* (Context::*)(const char*, int)>(_SC("wr_rd"), &Context::wr_rd);

    myClass.Overload<bool (Context::*)(const char*)>(_SC("write"), &Context::write);
    myClass.Overload<bool (Context::*)(const char*,const char*)>(_SC("write"), &Context::write);
    myClass.Overload<bool (Context::*)(const char*,const char*, int)>(_SC("write"), &Context::write);

    myClass.Overload<const char* (Context::*)(const char*, int)>(_SC("read"), &Context::read);
    myClass.Overload<const char* (Context::*)(const char*)>(_SC("read"), &Context::read);
    myClass.Overload<const char* (Context::*)()>(_SC("read"), &Context::read);

    myClass.Overload<bool (Context::*)(const char*, const char*,int)>(_SC("expect"), &Context::expect);
    myClass.Overload<bool (Context::*)(const char*,const char*)>(_SC("expect"), &Context::expect);
    myClass.Overload<bool (Context::*)(const char*)>(_SC("expect"), &Context::expect);
    myClass.Overload<bool (Context::*)()>(_SC("expect"), &Context::expect);

    myClass.Overload<bool (Context::*)()>(_SC("is_steady"), &Context::is_steady);
    myClass.Overload<bool (Context::*)(int)>(_SC("is_steady"), &Context::is_steady);

    myClass.Func(_SC("is_ready"), &Context::is_ready);

    myClass.Overload<bool (Context::*)()>(_SC("io_flush"), &Context::io_flush);
    myClass.Overload<bool (Context::*)(int)>(_SC("io_flush"), &Context::io_flush);

    myClass.Overload<const char* (Context::*)(const char*,const char*,int)>(_SC("mlwrite"), &Context::mlwrite);
    myClass.Overload<const char* (Context::*)(const char*,const char*)>(_SC("mlwrite"), &Context::mlwrite);
    myClass.Overload<const char* (Context::*)(const char*)>(_SC("mlwrite"), &Context::mlwrite);


    //myClass.Func(_SC("write"), &Context::write);
    myClass.Func(_SC("login"), &Context::login);
    myClass.Func(_SC("logout"), &Context::logout);
    myClass.Func(_SC("send_key"), &Context::send_key);
    myClass.Func(_SC("sleep"), &Context::sleep);
    myClass.Func(_SC("io_clear"), &Context::io_clear);
    myClass.Func(_SC("exit"), &Context::exito);
    myClass.Func(_SC("set_timeout"), &Context::set_timout);
    myClass.Func(_SC("get_input"), &Context::get_input);
    myClass.Func(_SC("put_file"), &Context::put_file);
    myClass.Func(_SC("get_file"), &Context::get_file);
    myClass.Func(_SC("log_status"), &Context::log_status);
    myClass.Func(_SC("get_prompt"), &Context::get_prompt);
    myClass.Func(_SC("re_login"), &Context::re_login);
    myClass.Func(_SC("get_uid"), &Context::get_uid);
    myClass.Func(_SC("show_term"), &Context::show_term);
    myClass.Func(_SC("monitor"), &Context::monitor);
    myClass.Func(_SC("pick"), &Context::pick);
    myClass.Func(_SC("download"), &Context::download);
    myClass.Func(_SC("send_passwd"), &Context::send_passwd);
    myClass.Func(_SC("report"), &Context::report);

    Sqrat::RootTable().Bind(_SC("Term"), myClass);
}

