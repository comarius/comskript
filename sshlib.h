/*
    Author: Marius Octavian Chincisan, Jan-Aug 2012
    Copyright: Marius C.O.
*/
#ifndef SSSH_X_CLASS_H
#define SSSH_X_CLASS_H

#include <iostream>
#include <stdlib.h>
#include <string>
#include <config.h>
#include "sqwrap.h"
#include "globalfoos.h"
#include <libssh/libssh.h> //sudo apt-get install libssh-dev
#include <libssh/sftp.h>
#include <libssh/server.h>
#include "remotetty.h"

using namespace std;


class SSh : public remotetty
{
public:
    SSh();
    virtual ~SSh();
    eNUMEXP connect(const char* user, const char* pass, const char* host, int port, const string& args);
    eNUMEXP connect();
    eNUMEXP open_tty();
    eNUMEXP receive(string& s);
    eNUMEXP send(const string& c);
    eNUMEXP put_file(const char* local, const char* remote);
    eNUMEXP get_file(const char* remote, const char* local);
    bool isopen();
    void close();
private:
    eNUMEXP _verify_knownhost();

private:
    ssh_session _ssh_session;
    ssh_channel _channel;
};
#endif // SQCLASS_H
