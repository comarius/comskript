#ifndef BASHOXE_H
#define BASHOXE_H


#include <string>
#include "sqwrap.h"


using namespace std;
class bashoxe
{
public:
    bashoxe():_bash(){};
    virtual ~bashoxe();
    const string& bash()const{return _bash;}
    bool load(const char* s);
    static void sq_export(SqEnv& env);
protected:
private:
    string _bash;
};

#endif // BASHOXE_H
