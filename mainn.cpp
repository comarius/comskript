
#include <signal.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <pwd.h>
#include <sys/termios.h>
#include <fcntl.h>
#include <procinst.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "sqwrap.h"
#include "sock.h"
#include "concontext.h"
#include "iothread.h"
#include "fxngine.h"
#include "nutconf.h"

using namespace std;

int         P_INSTS = 0;
pid_t       P_ID;
void        ControlC (int i);
procinst    Inst(SIGINT|SIGABRT|SIGKILL, SIGTRAP|SIGPIPE|SIGBUS, ControlC);

void ControlC (int i)
{
    Inst.kill(0,0);
    ::sleep(1);
}

void    ChildExits(int i)
{
    if(SIGCHLD==i)
    {
        if(P_INSTS>0)
            --P_INSTS;
    }
};



//=============================================================================
int main(int nargs, char* vargs[])
{
    Conf::__flushra = true;
    nutconf   f("comskript.conf");
    P_ID = getpid();
#ifdef DEBUG
   // system("rm -rf ../logs/*");
    system("rm -rf /tmp/f*");
#endif //DEBUG
    char buf[128];
    getlogin_r(buf,sizeof(buf)-1);
    GLOGI(" ==main enters== " << vargs[0] << ", " << buf );

    if(-1==Inst.instance(nargs,vargs,"n",false,false))
    {
        GLOGW("exiting due stop file in /tmp");
        return -1;
    }
    Inst.reap_zobies(ChildExits);
    fxngine  n;
    n.run(nargs, vargs);
    sleep(1);
    GLOGI("main exit\n" << vargs[0]);
    sleep(5);

    return 0;
}





