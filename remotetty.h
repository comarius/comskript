#ifndef REMOTETTYX_H
#define REMOTETTYX_H

#include <os.h>
#include <string>
#include "nutconf.h"
#include "testate.h"
using namespace std;


//--------------------------------------------------------------------------------------------
class remotetty
{
public:
    remotetty();
    virtual ~remotetty();

    virtual eNUMEXP connect(const char* user, const char* pass, const char* host, int port, const string& args)=0;
    virtual eNUMEXP connect()=0;
    virtual eNUMEXP open_tty()=0;
    virtual eNUMEXP receive(string& s)=0;
    virtual eNUMEXP send(const string& c)=0;
    virtual eNUMEXP put_file(const char* src, const char* dst)=0;
    virtual eNUMEXP get_file(const char* src, const char* dst)=0;
    virtual bool     isopen()=0;
    virtual void     close()=0;
    const string&    prompt()const{return _prompt;}
    time_t           response_time()const{return _resptime;}
    const string&    exname()const{return _extraname;}
protected:
    virtual eNUMEXP _open_tty(nutconf::protocol& proto);
    bool    _transact(const char* cmd, string& s);
    void    _flush();
protected:
    string      _prompt;
    string      _user;
    string      _pass;
    string      _host;
    int32_t     _port;
    string      _args;
    string      _extraname;
    int32_t     _devport;
    time_t      _resptime;
};

#endif // REMOTETTY_H
