

#include "adbtty.h"
#include "main.h"

adbtty::adbtty():sockimpl(_c)
{
    if(!PNCONF->_adb.prestart.empty())
    {
        string cmd = PNCONF->_adb.prestart;
        if(cmd.find("\r")==string::npos)cmd+="\r";
        ::system(cmd.c_str());
        ::sleep(1);
    }
}

adbtty::~adbtty()
{
    //dtor
}

bool adbtty::_adb_send(const char* buffer)
{
    uint8_t   tmp[8] = {0};
    size_t len = ::strlen(buffer);

    ::snprintf((char*)tmp, sizeof(tmp), "%04x", (int)len);
    tmp[4]=0;
    return _esend(tmp,4) && _esend((uint8_t*)buffer, len);
}

bool adbtty::_adb_receive(string& s)
{
    char buff[512]={0};

    if(_ereceive((uint8_t*)buff,4))
    {
        uint32_t n = strtoul(buff, 0, 16);
        if(n<sizeof(buff)-1)
        {
            if(_ereceive((uint8_t*)buff, n))
            {
                buff[n]=0;
                s=buff;
                return true;
            }
        }
    }
    return false;
}


bool adbtty::_adb_status()
{
    uint8_t buf1[255]={0};

    if(!_ereceive(buf1, 4))
    {
        GLOGE("adb protocol fault (no status)");
        return false;
    }
    GLOGT("adb status: " << buf1);
    if(!::memcmp(buf1, "OKAY", 4))
    {
        return true;
    }

    if(::memcmp(buf1, "FAIL", 4))
    {
        GLOGE("adb protocol fault status :FAIL");
        return false;
    }
    string s;
    _adb_receive(s);
    GLOGE("adb last response: " << s);
    return false;
}

eNUMEXP adbtty::connect(const char* user, const char* pass, const char* host, int port, const string& args)
{
    _user = user;
    _pass = pass;
    _host = host;
    _port = port;
    _args= args;

    size_t nhost = ::atol(host);
    _c.destroy();

    if(nhost < 128 && nhost>=0) //is the index of the device in adb devices
    {
         string sdev("host:devices-l");
        _c.set_blocking(1);
        if( _c.create(port) &&
            _c.try_connect("127.0.0.1",port) &&
            _adb_send(sdev.c_str()))
        {
            string  s;
            size_t  ordinal=1;
            char    resp[8]={0};
            _ereceive((uint8_t*)resp,4);
            if(resp[0]=='O') //OKAY
            {
                _adb_receive(s);
                GLOGT(s);
                if(s.find(" ") != string::npos)
                {
                    stringstream ss(s);
                    string line;
                    while(getline(ss, line, '\n'))
                    {
                        if(nhost==ordinal)
                        {
                            stringstream ss(line);
                            string token;
                            while(getline(ss, token, ' '))
                            {
                                if(_host.length()<4)
                                {
                                    _host=token;
                                }
                                _extraname=token;
                            }
                            break;
                        }
                        ++ordinal;
                    }
                }
                else
                {
                    GLOGE("cannot get device list. restart adb-seerver");
                    return EX_OPENTTY;
                }
            }

        }
        _c.destroy();
        ::msleep(100);
    }


    string sdev("host:transport:"); sdev+=_host;
    _c.set_blocking(1);

    if(_c.create(port) &&
        _c.try_connect("127.0.0.1",port) &&
        _adb_send(sdev.c_str()))
    {
        char okay[8]={0};

        _ereceive((uint8_t*)okay,4);
        GLOGT("connect: "<< okay);
        if(okay[0]=='O' && _adb_send("term:"))
        {
             _ereceive((uint8_t*)okay,4);
             if(okay[0]=='O')
             {
                string s;
                _receive(s);
                GLOGE(s);
                _c.set_blocking(0);
                return EX_NONE;
            }
        }
    }
    return EX_OPENTTY;
}

eNUMEXP adbtty::connect()
{
    return connect(_user.c_str(), _pass.c_str(), _host.c_str(), _port, _args);
}

eNUMEXP adbtty::open_tty()
{
    eNUMEXP rv = _open_tty(PNCONF->_adb);
    return rv;
}

eNUMEXP adbtty::receive(string& s)
{
    return _receive(s);
}

eNUMEXP adbtty::send(const string& c)
{
    //return _adb_send(c.c_str())==true ? EX_NONE : EX_CONBROKEN;
    return _c.sendall(c.c_str(),c.length())==0 ? EX_NONE: EX_CONBROKEN;
}

eNUMEXP adbtty::put_file(const char* src, const char* dst)
{
    //adb -s $SERIAL push
    stringstream s;
    s << "adb push -s " << _host << src <<" "<< dst << "\r";
    system(s.str().c_str());
    return EX_NONE;
}

eNUMEXP adbtty::get_file(const char* src, const char* dst)
{
    stringstream s;
    s << "adb pull -s " << _host << dst <<" "<<src << "\r";
    system(s.str().c_str());
    return EX_NONE;
}


bool    adbtty::isopen()
{
    return _c.socket() > 0;
}

void    adbtty::close()
{
    if(!PNCONF->_adb.exitstring.empty())
        send(PNCONF->_ooh.exitstring.c_str());
    else{
        GLOGW("there is no  exit string on adbtty. using exit");
        send("exit\r\n");
    }
}

