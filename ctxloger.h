#ifndef CTXLOGER_H
#define CTXLOGER_H

#include <strutils.h>
#include "nutconf.h"
#include "concontext.h"

class Context;
class ctxloger
{
public:
    ctxloger(Context* ctx)
    {
        _parent = ctx->basefolder();
        _fname  =  _parent + ctx->_fuid();
        _fname += ".log0";

        std::ofstream ofs;
        ofs.open( _fname.c_str(), ios::out | ios::app );
        ofs.close();
        _len = 0;
    };
    virtual ~ctxloger()
    {
        flush();
    };

    template <typename T> ctxloger& operator << (const T& log)
    {
        std::ofstream ofs;
        ofs.open( _fname.c_str(), ios::out | std::ofstream::trunc );
        ofs << log;
        _len = ofs.tellp();
        ofs.close();
        return *this;
    }

    void flush()
    {
        if(_len > 2000000) //2M log
        {
            int         iold = 3;
            string      oldFn = _parent + _fname.substr(0,_fname.find("."));
            oldFn += ".log";
            oldFn += std::to_string(4);

            ::unlink(oldFn.c_str());
            while(iold-- > 0)
            {
                string      oldFn = _parent + _fname.substr(0,_fname.find(".")); oldFn += "log"; oldFn += std::to_string(iold);
                string      newFn = _parent + _fname.substr(0,_fname.find(".")); oldFn += "log"; oldFn += std::to_string(iold-1);
                ::rename(oldFn.c_str(), newFn.c_str());
            }
        }
    }

private:
    std::string     _fname;
    std::string     _parent;
    size_t          _len;
};


#ifdef DEBUG

#define CLOGI(x) if(_blog & 0x1) \
do{\
    ctxloger c(this);   \
    std::cout << str_time() <<" I:"<<this<<": " << x << "\n"; \
    c << str_time() <<" I:"<<this<<": " << x << "\n"; c.flush();\
}while(0);

#define CLOGW(x) if(_blog & 0x2) \
do{\
    ctxloger c(this);   \
    std::cout << str_time() <<" W:" <<this<<": " << x << "\n"; \
    c << str_time() <<" W:"<<this<<": " << x << "\n"; c.flush();\
}while(0);

//-----------------------------------------------------------------------------
#define CLOGE(x) if(_blog & 0x4) \
do{\
    ctxloger c(this);   \
    std::cout << str_time() <<" E:"<<this<<": " << x << "\n";\
    c << str_time() <<" E:"<<this<<": " << x << "\n"; c.flush();\
}while(0);

#define CLOGT(x) if(_blog & 0x8) \
do{\
    ctxloger c(this);   \
    std::cout << str_time() <<" T:"<<this<<": " << x << "\n"; \
    c << str_time() <<" T:"<<this<<": " << x << "\n"; c.flush();\
}while(0);

#define CLOGD(x) if(_blog & 0x10) \
do{\
    ctxloger c(this);   \
    std::cout << str_time() <<" D:"<<this<<": " << x << "\n"; \
    c << str_time() <<" D:"<<this<<": " << x << "\n"; c.flush();\
}while(0);

#define CLOGX(x) if(_blog & 0x20) \
do{\
    ctxloger c(this);   \
    std::cout << str_time() <<" X:"<<this<<": " << x << "\n";\
    c << str_time() <<" X:"<<this<<": " << x << "\n"; c.flush();\
}while(0);

#else


#define CLOGI(x) if(_blog & 0x1) \
do{\
    ctxloger c(this);   \
    c << str_time() <<" I:"<<this<<": " << x << "\n"; c.flush();\
}while(0);


#define CLOGW(x) if(_blog & 0x2) \
do{\
    ctxloger c(this);   \
    c << str_time() <<" W:"<<this<<": " << x << "\n"; c.flush();\
}while(0);

//-----------------------------------------------------------------------------
#define CLOGE(x) if(_blog & 0x4) \
do{\
    ctxloger c(this);   \
    c << str_time() <<" E:"<<this<<": " << x << "\n"; c.flush();\
}while(0);

#define CLOGT(x) if(_blog & 0x8) \
do{\
    ctxloger c(this);   \
    c << str_time() <<" T:"<<this<<": " << x << "\n"; c.flush();\
}while(0);

#define CLOGD(x) if(_blog & 0x10) \
do{\
    ctxloger c(this);   \
    c << str_time() <<" D:"<<this<<": " << x << "\n"; c.flush();\
}while(0);


#define CLOGX(x) if(_blog & 0x20) \
do{\
    ctxloger c(this);   \
    c << str_time() <<" X:"<<this<<": " << x << "\n"; c.flush();\
}while(0);



#endif





#endif // CTXLOGER_H
