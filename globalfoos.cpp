/*
    Author: Marius Octavian Chincisan, Jan-Aug 2012
    Copyright: Marius C.O.
*/
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string>
#include <fstream>
#include <streambuf>
#include "strutils.h"
#include "main.h"
#include "nutconf.h"
#include "fxngine.h"
#include "concontext.h"
#include "globalfoos.h"

static SQInteger _mod_spawn(HSQUIRRELVM v)
{
    const SQChar    *fname;
    const SQChar    *cred;
    const SQChar    *mid;

    sq_getstring(v,2,&fname);
    sq_getstring(v,3,&cred);
    sq_getstring(v,4,&mid);
    return PFX->run_inst(fname, cred, mid)==true ? 1 : 0;
}

static SQInteger _mod_sleep(HSQUIRRELVM v)
{
    SQInteger    fname;
    sq_getinteger(v,2,&fname);

    GLOGT(__FUNCTION__<<": " << fname);
    int seconds = fname;
    while(Inst.alive() && --seconds>0)
    {
        ::sleep(1);
    }
    return 1;
}

static SQInteger _mod_msleep(HSQUIRRELVM v)
{
    SQInteger    fname;
    sq_getinteger(v,2,&fname);

    GLOGT(__FUNCTION__<<": " << fname);

    int mseconds = fname;
    while(Inst.alive() && --mseconds>0)
    {
        ::msleep(1);
    }
    return 1;
}


static SQInteger _mod_rsleep(HSQUIRRELVM v)
{
    SQInteger    fname;
    sq_getinteger(v,2,&fname);

    GLOGT(__FUNCTION__<<": " << fname);

    int mseconds = (rand() % fname) + 1;
    while(Inst.alive() && --mseconds>0)
    {
        ::msleep(1);
    }
    return 1;
}

static SQInteger _mod_setlog(HSQUIRRELVM v)
{
    const SQChar    *log;

    sq_getstring(v,2,&log);
    string sslog = log;
    PNCONF->_blog=0;
    PNCONF->_blog |= sslog.find('I') == string::npos ? 0 : 0x1;
    PNCONF->_blog |= sslog.find('W') == string::npos ? 0 : 0x2;
    PNCONF->_blog |= sslog.find('E') == string::npos ? 0 : 0x4;
    PNCONF->_blog |= sslog.find('T') == string::npos ? 0 : 0x10;
    PNCONF->_blog |= sslog.find('D') == string::npos ? 0 : 0x8;
    PNCONF->_blog |= sslog.find('X') == string::npos ? 0 : 0x20;
    PNCONF->_blog |= sslog.find('H') == string::npos ? 0 : 0x40;
    PNCONF->_blog |= sslog.find('A') == string::npos ? 0 : 0xFFFF;

    return 1;
}




static SQInteger _mod_run(HSQUIRRELVM v)
{
    const SQChar    *fname;
    const SQChar    *cred;
    const SQChar    *mid;
    char lococred[128] ={0};

    sq_getstring(v,2,&fname);
    sq_getstring(v,3,&cred);
    sq_getstring(v,4,&mid);

    GLOGT("run " << fname << cred << mid);

    if(cred[0]=='@')
        ::strcpy(lococred, cred+1);
    else
        ::strcpy(lococred, cred);

    if(!PFX->credentials().empty() && cred[0]!='@')
    {
        ::strcpy(lococred, PFX->credentials().c_str());
    }

    PFX->sq_run(lococred, mid, PFX->getsq_env(), fname);
    return 1;
}

static SQInteger _mod_exit(HSQUIRRELVM v)
{
    throw (1);
    return 1;
}

#define _DECL_FUNC(name,nparams,pmask) {_SC(#name),_mod_##name,nparams,pmask}

static SQRegFunction global_funcs[]=
{
    _DECL_FUNC(spawn,4,_SC(".sss")),
    _DECL_FUNC(run,4,_SC(".sss")),
    _DECL_FUNC(sleep,2,_SC(".i")),
    _DECL_FUNC(msleep,2,_SC(".i")),
    _DECL_FUNC(rsleep,2,_SC(".i")),
    _DECL_FUNC(exit,1,_SC("")),
    _DECL_FUNC(setlog,2,_SC(".s")),
    {0,0,0,0}
};

#undef _DECL_FUNC


SQInteger sq_export(HSQUIRRELVM v) {
    SQInteger i=0;
    SQRegFunction* pfpps = global_funcs;
    while(pfpps[i].name!=0) {
        sq_pushstring(v,pfpps[i].name,-1);
        sq_newclosure(v,pfpps[i].f,0);
        sq_setparamscheck(v,pfpps[i].nparamscheck,pfpps[i].typemask);
        sq_setnativeclosurename(v,-1,pfpps[i].name);
        sq_newslot(v,-3,SQFalse);
        i++;
    }
/*
    aligne them with these enumerations
    EX_NONE=0.
    EX_CONNECT=1,
    EX_NETWORK
    EX_OPENTTY=1,
    EX_AUTH=2,
    EX_TIMEOUT,
    EX_USER,
*/


    return 1;
}
