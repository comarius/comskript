#ifndef TESTATE_H
#define TESTATE_H


#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <squirrel.h>

using namespace std;

SQInteger sq_export_err(HSQUIRRELVM v);

extern const char* c_states[32];
extern const char* c_exceptions[32];
//-----------------------------------------------------------------------------
enum C_PROGRESS{
    CP_STOPPED=0,
    CP_STARTING,
    CP_LOGGINGIN,
    CP_INSCRIPT,
    CP_RUNNING,
    CP_OFFLINE,
    CP_ONLINE,
    CP_INPUT,
    CP_ERROR,
    CP_ERR_SCRIPT,
    CP_WARNING,
    CP_FLUSHING,
    CP_DEADLOCK,
    CP_PAUSED,
    CP_SUCCESS,
    CP_BREAK,
    CP_INTER,
};
//-----------------------------------------------------------------------------
enum USER_DATA_IDX
{
    CTX_CFG=0,
    CTX_HOLDER=100,
};

enum eNUMEXP
{
    EX_NONE=0,
    EX_CONNECT,
    EX_NETWORK,
    EX_OPENTTY,
    EX_AUTH,
    EX_TIMEOUT,
    EX_CONBROKEN,
    EX_OFFLINE,
    EX_CONRESORED,
    EX_RESOURCE,
    EX_SSH,
    EX_USER,
    EX_POOLING,
    EX_SCRIPT,
    EX_NOTSUPORTED,
};


class Context;
class ctxerr
{
public:
    ctxerr(const string& bf, eNUMEXP code, Context* pc):_logfile(bf),_code(code),_pcdefunct(pc){
        _logcntn << code << ": " << c_exceptions[code];
    }
    ctxerr(const ctxerr& o){
        _logcntn << o._logcntn.str();
        _logfile = o._logfile;
        _code = o._code;
        _pcdefunct=o._pcdefunct;
    }
    ~ctxerr(){};
    template <class T>
    ctxerr&  operator << (const T& t){
        _logcntn << t;
        return *this;
    }
    const ctxerr& object()const;
    eNUMEXP code()const{return _code;};
    const string str(){return _logcntn.str();}
    string logfld(const char* sub)const{return _logfile + sub;}
    const uint64_t defunct()const{return uint64_t(_pcdefunct);}
private:
    string              _logfile;
    eNUMEXP             _code;
    std::stringstream   _logcntn;
    Context*            _pcdefunct;
};


//=============================================================================
class Context;
class Progress
{
    Context         *_pc;
    bool            _enabled;
public:
    Progress(Context* c, C_PROGRESS st,
            const stringstream& prog);
    ~Progress();
    void prog(C_PROGRESS st, const stringstream& prog);
    void disable(){_enabled=false;}
};


//-----------------------------------------------------------------------------
#define EMPTY    " "
#define _OC(x)   "("<< x <<")"
#define _O(x)    "("<<x
#define _P(x)    ","<<x
#define _C(x)    ","<<x<<")"
#define _U(x)   (x==0?"_":x)

//-----------------------------------------------------------------------------
#define PROG(state, scrf)                   \
do{                                         \
    GLOGD("st: " << c_states[state] <<": " << scrf );        \
}while(0);                                  \
stringstream __prog; __prog << scrf;        \
Progress __thep(this, state, __prog);

#define _PROG(state, args)                 \
do{                                        \
    GLOGD("st: " << state << args);        \
}while(0);                                 \
do{                                        \
    __prog << args;                        \
    __thep.prog(state, __prog);            \
}while(0);

#define _PROGOFF()  __thep.disable();

//-----------------------------------------------------------------------------
#define THROW(code, message)                                    \
do{                                                             \
    GLOGE(code << "" << message)                                \
    {\
        stringstream ss; ss << code << "," << message;           \
        GLOGD(code << ", " << message);                           \
        this->log_prog(CP_ERROR, ss);             \
    }\
    fwriteline(_ferror(), message);                             \
    int                 sqret = code;                           \
    ctxerr              exerr(_logpath,(eNUMEXP)code,this);     \
    _exit_state = CP_ERROR;                                 \
    _exit_msg = c_exceptions[code];                         \
    exerr <<" " << message;                                 \
    string  foo("on_"); foo += c_exceptions[code];          \
    Sqrat::Function func = Sqrat::RootTable().GetFunction(_SC(foo.c_str())); \
    if(!func.IsNull())                          \
    {                                           \
        sqret = func.Evaluate<int>(this);       \
    }                                           \
    else                                        \
        CLOGW("function " << foo <<"()" << " not found");\
    if(sqret != EX_NONE)            \
    {                               \
        throw(exerr.object());      \
    }                               \
}while(0);


#endif // TESTATE_H
