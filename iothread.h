/*
    Author: Marius Octavian Chincisan, Jan-Aug 2012
    Copyright: Marius C.O.
*/
#ifndef __OBJFOOS_FOOS_H_
#define __OBJFOOS_FOOS_H_

#include <fcntl.h>
#include <sys/stat.h>
#include <string>
#include <iostream>
#include <fstream>
#include <iosfwd>
#include <deque>
#include "strutils.h"
#include "os.h"
#include "sock.h"
#include "sshlib.h"
#include "testate.h"
#include "remotetty.h"
#include "oohcli.h"


using namespace std;



//-----------------------------------------------------------------------------
class SyncQueue
{
public:
    SyncQueue():_huge(false),_offset(0){
        char fname[256];
        ::sleep(1);
        ::sprintf(fname,"/tmp/qio-flexte-%d-%lld", getpid(), ((long  long int)this));
        _fn = fname;
    }
    ~SyncQueue(){
        clear();
    }
    void set_huge(){_huge=true;}
    size_t size()
    {
        AutoLock a(&_m);
        if(_huge)
        {
#ifdef F_FIFO
            _fifo

#else
            ifstream file (_fn.c_str(), ios::in|ios::binary|ios::ate);
            if (file.is_open())
            {
                size_t fsize = file.tellg();
                file.close();
                return fsize;
            }
#endif
            return 0;
        }
        return _q.length();
    }

    void clear()
    {
        AutoLock a(&_m);
        if(_huge)
        {
            ::fclear(_fn);
            _offset=0;
            ::unlink(_fn.c_str());
        }
        _q.clear();
    }

    bool enqueue(const string& c)
    {
        AutoLock a(&_m);
        if(_huge)
        {
            ofstream myfile (_fn.c_str(),ios::out | ios::app);
            if(myfile.is_open())
            {
                myfile << c;
                myfile.close();
                return true;
            }
            return false;
        }
        _q.append(c);
        return _q.size()>0;
    }

    int dequeue(string& rv)
    {
        int br = 0;

        AutoLock a(&_m);
        if(_huge)
        {
            if(!::access(_fn.c_str(),0)==0)
            {
                return 0;
            }
            ifstream file (_fn.c_str(), ios::in|ios::binary|ios::ate);
            if (file.is_open())
            {
                char      giveout[512];

                file.seekg (0, ios::end);
                size_t fsize = file.tellg();
                if(fsize==0) {
                    ::unlink(_fn.c_str());
                    return 0;
                }
                file.seekg (_offset, ios::beg);
                size_t left = fsize -  _offset;
                if(left < sizeof(giveout))
                {
                    file.read(giveout, left);
                    _offset=0;
                    file.close();
                    ::unlink(_fn.c_str());
                    rv.append(giveout);
                    br=1; // done
                }
                else
                {
                    file.read(giveout, sizeof(giveout));
                    _offset+=sizeof(giveout);
                    br=true;
                    rv.append(giveout);
                    br=1; //more data
                }
            }
            return br;
        }

        if(_q.length())
        {
            br = 1;
           rv.append(_q);
            _q.clear();
        }
        return br;
    }

private:
    string      _q;
    mutex       _m;
    bool        _huge;
    size_t      _offset;
    string      _fn;
    int         _fifo;
};

//-----------------------------------------------------------------------------
class Context;
class ttyevent;
class IoThread : public OsThread
{
public:
    static void chekin();


    IoThread(ttyevent*  pevent);
    ~IoThread();

    eNUMEXP connect_tty(const string& protocol, const string& user, const string& pass, const string& host, int port, const string& args);
    string setup_prompt();
    void reinit()
    {
        _fromuser.clear();
        _touser.clear();
        delete _ptty;
        _ptty=0;
    }
    bool isopen();
    const string& get_prompt()const{return _prompt;}
    bool sh_write(const string& s, int wait_send=8)//->input
    {
        _fromuser.enqueue(s);

        if(wait_send)
        {
            time_t next = time(0) + wait_send;
            while(time(0) < next && input_length())
            {
                ::msleep(800);
            }
            return input_length()==0;
        }
        return true;
    }

    int    sh_read(string& s, int wait_rec=8)//<-output
    {
        ::msleep(100);
        if(wait_rec)
        {
            time_t next = time(0) + wait_rec;
            while(time(0) < next && 0==response_length())
            {
                ::msleep(1000);
            }
        }
        return  _touser.dequeue(s);
    }
    size_t  response_length()
    {

        return _touser.size();
    }
    size_t input_length()
    {
        return _fromuser.size();
    }
    bool    flush(int secs);
    bool    to_shell(const string& s);
    bool    from_shell(string& s)
    {
        if(!_online){
            return 0;
        }
        return _touser.dequeue(s);
    }
    void set_timout(int to){_timeout=to;}
    eNUMEXP put_file(const char* l, const char* r);
    eNUMEXP get_file(const char* r, const char* l);
    void clear(){
        _touser.clear();
        _fromuser.clear();
    }

protected:
    virtual void thread_main();
    bool         _reconnect();
private:
    ttyevent*       _pevent;
    remotetty*      _ptty;
    SyncQueue       _touser;
    SyncQueue       _fromuser;
    time_t          _timeout;
    bool            _online;
    string          _prompt;
};

class ttyevent
{
public:
    ttyevent(){}
    virtual ~ttyevent(){}
    virtual void tty_event(eNUMEXP code)=0;
};

#endif //__GLOBAL_FOOS_H_
