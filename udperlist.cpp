

#include <sock.h>
#include "main.h"
#include "nutconf.h"
#include "udperlist.h"

udperlist::udperlist()
{
    //ctor
}

udperlist::~udperlist()
{
    //dtor
}


void udperlist::automatic()
{
    // when a client register start the next test
    udp_sock    srv;
    char        buff[512];
    int         port = PNCONF->_globo.port;

    if(srv.create(port) && srv.bind())
    {
        fd_set  r,w;
        struct timeval tv={0,0};

        srv.set_blocking(0);
        srv.set_option(SO_BROADCAST,1);
        srv.send("trigger", PNCONF->_globo.port, 0);
        srv.send("trigger", PNCONF->_globo.port+1, 0);
        srv.send("trigger", PNCONF->_globo.port+2, 0);

        GLOGI("waiting clients on port: " << PNCONF->_globo.port);

        FD_ZERO(&r);
        while(Inst.alive())
        {
            FD_SET(srv.socket(), &w);
            FD_SET(srv.socket(), &r);
            tv.tv_usec = 100000;
            int sel = ::select(srv.socket()+1,&r,&w,0,&tv);
            if(-1==sel)
            {
                GLOGE("Network select error:"<<errno<< ", Terminating this instance" );
                Inst.kill();
            }
            if(sel==0){
                ::msleep(256);
                if(ChildrenProcs<PNCONF->_globo.parralel && _tqueue.size())
                {
                    string tid = _tqueue.begin()->first;
                    string tip = _tqueue.begin()->second;
                    _tqueue.erase(_tqueue.begin());

                    //_test_target(tid,tip);
                }
                continue;
            }
            if(FD_ISSET(srv.socket(),&r))
            {

                SADDR_46    target;
                int bytes = srv.receive(buff, sizeof(buff)-1, target);
                if(bytes>0)
                {
                    buff[bytes]=0;
                    GLOGI("target: " << buff<<":"<<target.c_str()<<":"<<target.port() <<" added to test suite");
                    GLOGI("sending confirmation back to:" << target.c_str()<<":"<<target.port());

                    srv.send(buff, bytes, target);

//                    _test_target(string(buff), target.c_str());
                }
                FD_CLR(srv.socket(),&r);
            }
        }
    }
}
