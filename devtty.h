#ifndef DEVTTY_H
#define DEVTTY_H

#include "remotetty.h"


class devtty : public remotetty
{
public:
    devtty();
    ~devtty();
    virtual eNUMEXP connect(const char* user, const char* pass, const char* host, int port, const string& args);
    virtual eNUMEXP connect();
    virtual eNUMEXP open_tty();
    virtual eNUMEXP receive(string& s);
    virtual eNUMEXP send(const string& c);
    virtual eNUMEXP put_file(const char* src, const char* dst);
    virtual eNUMEXP get_file(const char* src, const char* dst);
    virtual bool     isopen();
    virtual void     close();
private:

    bool _set_options();

private:
    string  _device;
    int     _fd;
    int     _speed;
    int     _databits;
    int     _stopbits;
    string  _parity; // o n e

};

#endif // DEVTTY_H
