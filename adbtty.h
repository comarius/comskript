#ifndef ADBTTY_H
#define ADBTTY_H

#include "remotetty.h"
#include "sockimpl.h"

class adbtty : public remotetty, public sockimpl
{
public:
    adbtty();
    virtual ~adbtty();
    virtual eNUMEXP connect(const char* user, const char* pass, const char* host, int port, const string& args);
    virtual eNUMEXP connect();
    virtual eNUMEXP open_tty();
    virtual eNUMEXP receive(string& s);
    virtual eNUMEXP send(const string& c);
    virtual eNUMEXP put_file(const char* src, const char* dst);
    virtual eNUMEXP get_file(const char* src, const char* dst);
    virtual bool     isopen();
    virtual void     close();
protected:

    bool _adb_send(const char* buffer);
    bool _adb_receive(string& buffer);
    bool _adb_status();

private:

    tcp_cli_sock    _c;

};

#endif // ADBTTY_H
