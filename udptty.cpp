#include "udptty.h"

udptty::udptty():sockimpl(_srv)
{
    //ctor
}

udptty::~udptty()
{
    close();
}


eNUMEXP udptty::connect(const char* user, const char* pass, const char* host, int port, const string& args)
{
    _user = user;
    _pass = pass;
    _host = host;
    _port = port;
    _args= args;

    if(_srv.create(port) && _srv.bind(host))
    {
        return EX_NONE;
    }
    return EX_NETWORK;
}

eNUMEXP udptty::connect()
{
    return connect(_user.c_str(), _pass.c_str(), _host.c_str(), _port, _args);
}

eNUMEXP udptty::open_tty()
{
    eNUMEXP ex = _open_tty(PNCONF->_udp);
    if(ex == EX_NONE)
    {
        _transact("\r",_prompt);
        GLOGI("prompt is: " << _prompt);
    }
    return ex;
}

eNUMEXP udptty::receive(string& s)
{
    return _receive(s, &_remote);
}

eNUMEXP udptty::send(const string& c)
{
    return _send(c, &_remote);
}

eNUMEXP udptty::put_file(const char* src, const char* dst)
{
    return EX_RESOURCE;
}

eNUMEXP udptty::get_file(const char* src, const char* dst)
{
    return EX_RESOURCE;
}

bool     udptty::isopen()
{
    return _srv.socket()>0;
}

void     udptty::close()
{
    if(!PNCONF->_udp.exitstring.empty())
        _srv.send((uint8_t*)PNCONF->_ooh.exitstring.c_str(),PNCONF->_ooh.exitstring.length(),_remote);
    else{
        GLOGE("there is not  exit string on adbtty. using exit");
        _srv.send((uint8_t*)"exit\r\n",6, _remote);
    }
    _srv.destroy();
}
