
#include "main.h"
#include "iothread.h"
#include "logger.h"
#include "concontext.h"
#include "nutconf.h"
#include "remotetty.h"
#include "oohcli.h"
#include "sshlib.h"
#include "devtty.h"
#include "udptty.h"
#include "adbtty.h"
#include "usbtty.h"
#include "adbwifi.h"


IoThread::IoThread(ttyevent*  pevent):_pevent(pevent),_ptty(0),_online(true)
{
    _timeout = (PNCONF->_globo.cmdtime);
    _touser.set_huge();
}

IoThread::~IoThread()
{
    stop_thread();
    if(_ptty)
    {
        _ptty->close();
        delete _ptty;
    }
}

eNUMEXP  IoThread::connect_tty(const string& protoco, const string& user, const string& pass, const string& host, int port, const string& args)
{
    if(_ptty)
    {
        delete _ptty; _ptty = 0;
    }

    if(protoco=="ssh://")
        _ptty = (remotetty*)new SSh();
    else if(protoco=="ooh://")
        _ptty = (remotetty*)new oohcli();
    else if(protoco=="tty://")
        _ptty = (remotetty*)new devtty();
    else if(protoco=="udp://")
        _ptty = (udptty*)new udptty();
    else if(protoco=="adb://")
        _ptty = (udptty*)new adbtty();
    else if(protoco=="adw://")
        _ptty = (udptty*)new adbwifi();
    else
        return EX_NOTSUPORTED;

    eNUMEXP err = _ptty->connect(user.c_str(), pass.c_str(), host.c_str(), port, args);
    if(err!=EX_NONE)     return err;
    err = _ptty->open_tty();
    if(err != EX_NONE)   return err;

    _prompt     = _ptty->prompt();
    _timeout    = _ptty->response_time();

    return EX_NONE;
}


bool IoThread::isopen()
{
    _online = Inst.alive() && _ptty->isopen();
    return _online;
}

bool IoThread::flush(int secs)
{
    time_t fut = time(0) + secs;
    isopen();
    while(time(0)  < fut && _fromuser.size())
    {
        ::sleep(1);
    }
    if(_fromuser.size())
        GLOGW("flush timout");
    return _fromuser.size()==0;
}

bool    IoThread::to_shell(const string& s)
{
    isopen();
    if(!_online)
    {
        _fromuser.clear();
        GLOGW("remote shell is offline. ignoring: " << s);
        return false;
    }
    if(_fromuser.enqueue(s))
    {
        ::msleep(200);
        time_t fut  = time(0) + _timeout + 1;
        while(_fromuser.size() && time(0) < fut)
        {
            ::msleep(256);
        }
    }
    if(_fromuser.size())
    {
        GLOGW("faild to fush output to remote shell.");
        _pevent->tty_event(EX_CONBROKEN);
    }
    size_t sz =  _fromuser.size();
    return sz==0;
}


eNUMEXP IoThread::put_file(const char* l, const char* r)
{
    isopen();
    if(!_online)
    {
        GLOGW("remote shell is offline. ignoring: " << l);
        return EX_OFFLINE;
    }
    return _ptty->put_file(l,r);
}

eNUMEXP IoThread::get_file(const char* r, const char* l)
{
    isopen();
    if(!_online)
    {
        GLOGW("remote shell is offline. ignoring: " << l);
        return EX_OFFLINE;
    }
    return _ptty->get_file(r,l);
}

bool IoThread::_reconnect()
{
    isopen();
    _ptty->close();
    isopen();
    if(_ptty->connect() != EX_NONE)
    {
        ::sleep(PNCONF->_globo.retrytime);
        return false;
    }
    if(!_ptty->isopen())
    {
        ::sleep(PNCONF->_globo.retrytime);
        return false;
    }

    GLOGE("Reconnected to wrong same IP but there is a different machine. Test stopped");
    signal_to_stop();
    Inst.kill();
    return false;
}

void IoThread::thread_main()
{
    string  io;
    bool    on=true;

    GLOGT("====io thread starts");
    while(!_bstop && Inst.alive())
    {
        if(_ptty->isopen())
        {
            on=true;
            io.clear();
            if(_ptty->receive(io)==EX_NONE)
            {
                GLOGT("\n[io:recv]:" << io);
                _touser.enqueue(io);
            }
            io.clear();
            if(_fromuser.dequeue(io))
            {
                GLOGT("\n[io:send]:" << io);
                if(EX_NONE!=_ptty->send(io))
                {
                    GLOGE("cannot send to tty:" << io);
                    _ptty->close();
                    _pevent->tty_event(EX_CONBROKEN);
                }
            }
        }
        else
        {
            _touser.clear();
            if(on)
            {
                io="offline";
                _pevent->tty_event(EX_CONBROKEN);
                on=false;
            }
            ::msleep(1000);
            on = _reconnect();
        }
        usleep(0x1FFF);

    }
    GLOGT("====io thread ends");
}
