
#include "sockimpl.h"
#include "nutconf.h"
#include "main.h"

sockimpl::~sockimpl()
{
    //dtor
}

bool sockimpl::_ereceive(uint8_t* buff, int exact)
{
    bool b = _rc.receive(buff,exact)==exact;
    GLOGW("sk<" << buff);
    return b;
}

bool sockimpl::_esend(const uint8_t* buff, int exact)
{
    GLOGW("sk>" << buff);
    return _rc.sendall((const uint8_t*)buff,exact)==0;
}


eNUMEXP sockimpl::_receive(string& s,  SADDR_46* p)
{
    char    loco[1024];
    timeval tv    = {0, 132765};
    int     bytes = 0;
    fd_set  rset;
    int     ndfs;
    int     cycle = 0;

    if(!_rc.isopen())
    {
        return EX_CONBROKEN;
    }

    FD_ZERO(&rset);
    FD_SET(_rc.socket(),&rset);
    ndfs = _rc.socket()+1;

    int sel = ::select(ndfs, &rset, 0, 0, &tv);
    if(sel<0)
    {
        GLOGE("network error " << strerror(errno));
        if(++cycle > 8)
        {
            Inst.kill();
            return EX_NETWORK;
        }
        ::msleep(500);
        return EX_POOLING;
    }
    if(sel > 0)
    {
        if(FD_ISSET(_rc.socket(), &rset))
        {
            FD_CLR(_rc.socket(),&rset);
            if(p)
                bytes = _rc.receive(loco, sizeof(loco)-1,*p);
            else
                bytes = _rc.receive(loco, sizeof(loco)-1);

            if(bytes == 0)
            {
                GLOGE("network down" << errno);
                _rc.destroy();
                return EX_CONBROKEN;
            }
            if(bytes>0)
            {
                loco[bytes]=0;
                GLOGT("\nsk<" << loco);
                s=loco;
                return EX_NONE;
            }
        }
    }
    return EX_POOLING;
}

eNUMEXP  sockimpl::_send(const string& c,  SADDR_46* p)
{
    int bytes;
    if(p)
    {
        int sent = _rc.send(c.c_str(),c.length(), *p);
        if(sent>0)
        {
            bytes =- sent;
        }
    }
    else
    {
        bytes = _rc.sendall((const uint8_t*)c.c_str(),c.length());
    }
    GLOGT("sk> " << c);
    if(bytes == 0)
    {
        GLOGT("\n[sent]\n" << c);
        return EX_NONE;
    }
    return EX_CONBROKEN;
}



