#include <string>
#include <algorithm>
#include "main.h"
#include "nutconf.h"
#include "remotetty.h"

static int INITIAL_RESPT = 30;

remotetty::remotetty():_port(0),_devport(0),_resptime(INITIAL_RESPT)
{
}

remotetty::~remotetty()
{
    //dtor
}

bool    remotetty::_transact(const char* cmd, string& sret)
{
    string      resp;
    string      s;
    time_t      now = time(0);
    string      cmdnocr = cmd;

    sret.clear();
    sstr_replace(cmdnocr,'\r',' ');
    GLOGT("tr>> `"<<cmdnocr << "`");
    if(EX_NONE != this->send(string(cmd)))
    {
        GLOGE("comumnication error while opentty");
        return false;
    }
    ::msleep(2000);
    time_t fut = time(0) + _resptime;
    while(time(0) < fut && Inst.alive())
    {
        s.clear();
        if(EX_NONE==receive(s))
        {
            if(_resptime==INITIAL_RESPT){
                _resptime = (time(0) - now) + 3;
            }
            resp.append(s);
            if(resp.length()>256)
            {
                GLOGW("output is too long, Some program is in progress...");
                break;
            }
            fut = time(0) + _resptime;      //wait more
        }
        ::msleep(200);
    }
    if(resp.empty())
    {
        return false;
    }
    GLOGT("tr<< `"<<resp << "`");
    std::istringstream  iss(resp);
    std::string         token;
    string              lastline;

    if(((cmd[0]=='\r') && (resp=="\n" || resp=="\r\n" ||resp=="\r")) || (resp.length()>16384))
    {
        //we have a straight echo, a program must be stuck
        sret = "\r";
    }
    else
    {

        while(getline(iss, token, '\n'))
        {
            while(token.find('\r')!=string::npos)
                std::replace(token.begin(), token.end(),'\r',' ');

            if(cmdnocr==token)
                continue; // do not append the echo
            if(!_prompt.empty() && token.find(_prompt)!=string::npos)
                continue; // do not append the prompt
            lastline = token;
        }

        string::size_type pos = 0; // Must initialize
        while ( ( pos = lastline.find ("\r",pos) ) != string::npos )
        {
            lastline.erase ( pos, 1 );
        }
        pos=0;
        while ( ( pos = lastline.find ("\n",pos) ) != string::npos )
        {
            lastline.erase ( pos, 1 );
        }
        sret = lastline;
        if(sret.find("No such")!=string::npos || sret.find("denied")!=string::npos )
        {
            sret.clear();
        }
    }
    return !sret.empty();
}

/**
    try to get uniqie machine id from different places
*/
eNUMEXP remotetty::_open_tty(nutconf::protocol& proto)
{
    string rec;
    string prompt;

    _prompt.clear();

    if(!proto.initstring.empty())
    {
        if(!_transact(proto.initstring.c_str(), rec))
        {
            return EX_CONNECT;
        }
        _prompt = rec;
    }
    else //cr allways
    {
        if(!_transact("\r", rec))
        {
            return EX_CONNECT;
        }
        _prompt = rec;
    }
    return EX_NONE;
}

void    remotetty::_flush()
{
    string dummy;

    time_t fut = time(0) + PNCONF->_globo.cmdwait;
    ::msleep(500);
    while(this->receive(dummy)==EX_NONE && Inst.alive() && time(0)<fut)
    {
        GLOGT("flushing: " << dummy);
        fut = time(0) + PNCONF->_globo.cmdwait;
    }
}

