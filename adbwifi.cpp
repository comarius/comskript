

#include "adbwifi.h"
#include "main.h"

adbwifi::adbwifi():sockimpl(_c)
{
    if(!PNCONF->_adb.prestart.empty())
    {
        string cmd = PNCONF->_adb.prestart;
        if(cmd.find("\r")==string::npos)cmd+="\r";
        ::system(cmd.c_str());
        ::sleep(1);
    }
}

adbwifi::~adbwifi()
{
    //dtor
}


bool adbwifi::_adb_send(const char* buffer)
{
    uint8_t   tmp[8] = {0};
    size_t len = ::strlen(buffer);

    ::snprintf((char*)tmp, sizeof(tmp), "%04x", (int)len);
    tmp[4]=0;
    return _esend(tmp,4) && _esend((uint8_t*)buffer, len);
}

bool adbwifi::_adb_receive(string& s)
{
    char buff[512]={0};

    if(_ereceive((uint8_t*)buff,4))
    {
        uint32_t n = strtoul(buff, 0, 16);
        if(n<sizeof(buff)-1)
        {
            if(_ereceive((uint8_t*)buff, n))
            {
                buff[n]=0;
                s=buff;
                return true;
            }
        }
    }
    return false;
}

bool adbwifi::_adb_status()
{
    uint8_t buf1[255]={0};
    uint8_t buf[5]={0};
    unsigned len;

    if(!_ereceive(buf, 4))
    {
        GLOGE("adb protocol fault (no status)");
        return false;
    }
    GLOGT("adb status: " << buf);
    if(!::memcmp(buf, "OKAY", 4))
    {
        return true;
    }

    if(::memcmp(buf, "FAIL", 4))
    {
        GLOGE("adb protocol fault status :"<<  buf[0]<< buf[1]<< buf[2]<< buf[3]);
        return false;
    }

    if(!_ereceive(buf, 4))
    {
        GLOGE("protocol fault (status len)");
        return false;
    }
    buf[4]  = 0;
    len     = strtoul((char*)buf, 0, 16);
    if(len > 255) len = 255;
    if(!_ereceive( buf1, len))
    {
        GLOGE("protocol fault (status read)" << buf1);
    }
    return false;
}

eNUMEXP adbwifi::connect(const char* user, const char* pass, const char* host, int port,  const string& args)
{
    _user = user;
    _pass = pass;
    _host = host;
    _port = port;
    _args = args;

    /*
        tricly
        adw://IPOF_DEVICE:PORT_ADB_SERVER:DEVICE_IP
    */
    string  sdev("host:connect:");
            sdev+=_host;
            sdev+=":";
            sdev+=args;
    _c.destroy();
    _c.set_blocking(1);

    if(_c.create(port) &&
       _c.try_connect("127.0.0.1",port))
    {
        string s;
        if(!_adb_send(sdev.c_str()))    return EX_CONNECT; //host:connect
        if(!_adb_status())              return EX_CONNECT;
        if(!_adb_receive(s))            return EX_CONNECT;

        _c.destroy();
        if(_c.create(port) &&
            _c.try_connect("127.0.0.1",port))
        {
            ::msleep(100);

            if(!_adb_send("host:transport-any"))  return EX_CONNECT;
            if(!_adb_status())              return EX_CONNECT;

            if(!_adb_send("shell:"))  return EX_CONNECT;
            if(!_adb_status())              return EX_CONNECT;
            if(!_adb_receive(s))             return EX_CONNECT;
            return EX_NONE;
        }
    }
    return EX_CONNECT;
}

eNUMEXP adbwifi::connect()
{
    return connect(_user.c_str(), _pass.c_str(), _host.c_str(), _port, _args);
}

eNUMEXP adbwifi::open_tty()
{
    eNUMEXP rv = _open_tty(PNCONF->_adb);
    if(rv==EX_NONE)
    {
        _prompt.replace(0,4,"");
    }
    return rv;
}

eNUMEXP adbwifi::receive(string& s)
{
    return _receive(s);
}

eNUMEXP adbwifi::send(const string& c)
{
    //return _adb_send(c.c_str())==true ? EX_NONE : EX_CONBROKEN;
    return _c.sendall(c.c_str(),c.length())==0 ? EX_NONE: EX_CONBROKEN;
}

eNUMEXP adbwifi::put_file(const char* src, const char* dst)
{
    //adb -s $SERIAL push
    stringstream s;
    s << "adb push -s " << _host << src <<" "<< dst << "\r";
    system(s.str().c_str());
    return EX_NONE;
}

eNUMEXP adbwifi::get_file(const char* src, const char* dst)
{
    stringstream s;
    s << "adb pull -s " << _host << dst <<" "<<src << "\r";
    system(s.str().c_str());
    return EX_NONE;
}


bool    adbwifi::isopen()
{
    return _c.socket() > 0;
}

void    adbwifi::close()
{
    if(!PNCONF->_adb.exitstring.empty())
        send(PNCONF->_ooh.exitstring.c_str());
    else{
        GLOGE("there is not  exit string on adbwifi. using exit");
        send("exit\r\n");
    }
}

