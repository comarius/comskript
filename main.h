#ifndef MAIN_H_
#define MAIN_H

#include "procinst.h"
#include "strutils.h"

extern procinst  Inst;
extern int       P_INSTS;
extern pid_t     P_ID;



class Tempit
{
string _tmpfile;
bool   _created;
public:
    Tempit(const char* id):_tmpfile("/tmp/ft-run-"),_created(false){
        _tmpfile += id;
    };
    ~Tempit(){
        if(_created)
            ::unlink(_tmpfile.c_str());
    }
    bool valid(){
        if(::access(_tmpfile.c_str(),0)==0)
            return false;
        _created=true;
        ::fwriteline(_tmpfile, std::to_string(getpid()));
        ::chmod(_tmpfile.c_str(), S_IRWXU|S_IRWXG|S_IROTH|S_IWOTH);
        return true;
    }
};




#endif //MAIN_H
