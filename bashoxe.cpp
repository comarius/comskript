#include "bashoxe.h"

bashoxe::~bashoxe()
{
    //dtor
}

void bashoxe::sq_export(SqEnv& env)
{
    Sqrat::Class<bashoxe> myClass(env.theVM());

    myClass.Func(_SC("load"), &bashoxe::load);
    Sqrat::RootTable().Bind(_SC("Bash"), myClass);
}

bool bashoxe::load(const char* s)
{
    _bash=s;
    return true;
}
