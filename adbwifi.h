#ifndef ADBWIFI_H
#define ADBWIFI_H

#include "remotetty.h"
#include "sockimpl.h"

class adbwifi : public remotetty, public sockimpl
{
public:
    adbwifi();
    virtual ~adbwifi();
    virtual eNUMEXP connect(const char* user, const char* pass, const char* host, int port, const string& args);
    virtual eNUMEXP connect();
    virtual eNUMEXP open_tty();
    virtual eNUMEXP receive(string& s);
    virtual eNUMEXP send(const string& c);
    virtual eNUMEXP put_file(const char* src, const char* dst);
    virtual eNUMEXP get_file(const char* src, const char* dst);
    virtual bool     isopen();
    virtual void     close();
protected:

    bool _adb_send(const char* buffer);
    bool _adb_status();
    bool _adb_receive(string& s);
private:

    tcp_cli_sock    _c;

};

#endif // adbwifi_H
