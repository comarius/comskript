
#include <config.h>
#include "main.h"
#include "oohcli.h"
#include "globalfoos.h"
#include "nutconf.h"
#include "testate.h"

oohcli::oohcli():sockimpl(_c),_session(0),_cycle(0)
{
    close();
}

oohcli::~oohcli()
{
    //dtor
}

eNUMEXP oohcli::connect()
{
    return connect(_user.c_str(), _pass.c_str(), _host.c_str(), _port, _args);
}


eNUMEXP oohcli::connect(const char* user, const char* pass, const char* host, int port, const string& args)
{
    _user = user;
    _pass = pass;
    _host = host;
    _port = port;
    _args= args;

    _c.set_blocking(1);
    if(_c.create(0,0,0))
    {
        if(_c.try_connect(host, port))
        {
            _c.set_blocking(0);
            return EX_NONE;
        }
    }
    GLOGE("cannot conect to: " << host << ":" <<port);
    _c.destroy();
    return EX_CONNECT;
}


eNUMEXP oohcli::open_tty()
{
    eNUMEXP ex = _open_tty(PNCONF->_ooh);
    if(ex == EX_NONE)
    {
        _transact("\r",_prompt);
        GLOGI("prompt is: " << _prompt);
    }
    return ex;
}

eNUMEXP oohcli::receive(string& s)
{
    return _receive(s);
}

eNUMEXP  oohcli::send(const string& c)
{
    return _send(c);
}

bool oohcli::isopen()
{
    return _c.is_really_connected();
}

void oohcli::close()
{
    if(!PNCONF->_ooh.exitstring.empty())
        _c.sendall(PNCONF->_ooh.exitstring.c_str(),PNCONF->_ooh.exitstring.length());
    else{
        GLOGE("there is not  exit string on adbtty. using exit");
        _c.sendall("exit\r\n",6);
    }
    _c.destroy();
    _session = 0;
}

int oohcli::_read_header(char* buff, int bytes, uint32_t& remlen)
{
    char    loco[255];
    int     hdrlen = 0;
    char*   eohdr = str_up2chr(buff, '*', hdrlen);

    GLOGI(" received header[" << eohdr <<"]");

    ::strncpy(loco, eohdr, sizeof(loco)-1);
    const char* mustp = ::strtok(loco,":");

    char pg    = mustp[0];
    ::strtok(0,":");

    remlen     = ::atol(::strtok(0,":"));
    if(pg=='p' && remlen==0)
    {
        return 0;
    }
    return hdrlen;
}

eNUMEXP oohcli::put_file(const char* local, const char* remote)
{
    GLOGT(__FUNCTION__ << local <<","<<remote);
    FILE* pf = ::fopen(local,"rb");
    if(0==pf)
    {
        GLOGE("cannot open " << local);
        return EX_RESOURCE;
    }
    AutoCall<int (*)(FILE*), FILE*> ac(::fclose, pf);
    tcp_cli_sock    client;

    if(client.create(_port+1)<=0)
    {
        return EX_RESOURCE;
    }
    client.set_blocking(1);
    if(client.try_connect(_host.c_str(), _port+1)<=0)
    {
        GLOGE("cannot connect " << _host <<":" << (_port+1));
        return EX_CONNECT;
    }
    ::fseek (pf, 0, SEEK_END);   // non-portable
    size_t  len=ftell (pf);
    size_t  remain=len;
    ::fseek (pf, 0, SEEK_SET);   // non-portable
    char loco[16];
    ::sprintf(loco,"%ld", len);
    string putcmd="p:";
    putcmd+=remote;
    putcmd+=":";
    putcmd+=loco;
    putcmd+="*";
    GLOGT(putcmd);
    if(client.sendall(putcmd.c_str(), putcmd.length())!=0)
    {
        GLOGE("cannot send put header to " << _host <<":" << (_port+1));
        return EX_CONBROKEN;
    }
    ::usleep(0xFFFF);

    int     bytes,wrote;
    char    buff[4096];
    while(!::feof(pf))
    {
        bytes = ::fread(buff,1,sizeof(buff),pf);
        if(bytes>0)
        {
            wrote = client.sendall(buff,bytes);
            if(wrote!=0)
            {
                GLOGE("socket sendall:" << client.error());
                return EX_CONBROKEN;
            }
            remain-=bytes;
            GLOGT("sending: leftover:" << remain);
        }
        if(::feof(pf))
            break;
        ::msleep(100);
    }
    client.destroy();
    return remain==0 ? EX_NONE : EX_RESOURCE;
}



eNUMEXP oohcli::get_file(const char* remote, const char* local)
{
    GLOGT(__FUNCTION__ << remote <<","<<local);
    FILE* pf = ::fopen(local,"wb");
    if(0==pf)
    {
        GLOGE("cannot open " << local);
        return EX_RESOURCE;
    }
    AutoCall<int (*)(FILE*), FILE*> ac(::fclose, pf);
    tcp_cli_sock                    client;

    client.set_blocking(1);
    if(client.try_connect(_host.c_str(), _port+1)<=0)
    {
        GLOGE("cannot connect " << _host <<":" << (_port+1));
        return EX_CONNECT;
    }
    string putcmd="g:";
    putcmd+=remote;
    putcmd+=":0*";

    if(client.sendall(putcmd.c_str(), putcmd.length())!=0)
    {
        GLOGE("cannot send put header to " << _host <<":" << (_port+1));
        return EX_CONBROKEN;
    }
    ::usleep(0xFFFF);

    char        buff[4096];
    int         bytes = client.receive(buff,sizeof(buff)-1);
    int         written=0;
    if(bytes<=0)
    {
        GLOGE("cannot receive " << _host <<":" << (_port+1)  <<"," << remote);
        return EX_CONBROKEN;
    }
    uint32_t remlen = 0;
    int      hdrlen = _read_header(buff, bytes, remlen);
    if(0==hdrlen)
    {
        GLOGE("cannot receive header from" << _host <<":" << (_port+1) <<"," <<remote);
        return EX_CONBROKEN;
    }
    while(remlen)
    {
        bytes = client.receive(buff,sizeof(buff));
        if(bytes>0)
        {
            written = ::fwrite(buff,1,bytes,pf);
            if(written!=bytes)
            {
                return EX_CONBROKEN;
            }
            remlen-=written;
            GLOGT("receiving leftover:" << remlen);
        }else
            break;
    }

    return remlen==0 ? EX_NONE : EX_CONBROKEN;
}
