QT += core
QT -= gui

TARGET = comskript
CONFIG += console
CONFIG -= app_bundle

INCLUDEPATH += mcoutil
INCLUDEPATH += mcosocks
INCLUDEPATH += sq/include
INCLUDEPATH += sq/sqrat/include


QMAKE_CXXFLAGS += -std=c++11

TEMPLATE = app

SOURCES += \
    adbtty.cpp \
    adbwifi.cpp \
    bashoxe.cpp \
    concontext.cpp \
    devtty.cpp \
    exterm.cpp \
    fileterm.cpp \
    fxngine.cpp \
    globalfoos.cpp \
    iothread.cpp \
    mainn.cpp \
    nutconf.cpp \
    oohcli.cpp \
    remotetty.cpp \
    sockimpl.cpp \
    sqwrap.cpp \
    sshlib.cpp \
    testate.cpp \
    udptty.cpp \
    mcosocks/sock.cpp \
    mcoutil/config.cpp

DISTFILES += \
    hook_up_shell.sh \
    comskript.pro.user \
    readme.txt \
    sqmethods.inc \
    cmake_install.cmake

HEADERS += \
    adbtty.h \
    adbwifi.h \
    bashoxe.h \
    concontext_exp.h \
    concontext.cpp2 \
    concontext.h \
    consts.h \
    ctxloger.h \
    devtty.h \
    exterm.h \
    fileterm.h \
    fxngine.h \
    globalfoos.h \
    iothread.h \
    logger.h \
    main.h \
    mainn.h \
    matypes.h \
    nutconf.h \
    oohcli.h \
    remotetty.h \
    sockimpl.h \
    sqbind.h \
    sqmod.h \
    sqwrap.h \
    sshlib.h \
    testate.h \
    udperlist.h \
    udptty.h \
    usbtty.h \
    mcosocks/sock.h \
    mcoutil/config.h





win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/release/ -lsq
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/debug/ -lsq
else:unix: LIBS += -L$$PWD/lib/ -lsq

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/release/libsq.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/debug/libsq.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/release/sq.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/debug/sq.lib
else:unix: PRE_TARGETDEPS += $$PWD/lib/libsq.a


#LIBS += pthread dl ssh util

unix|win32: LIBS += -lpthread
unix|win32: LIBS += -lssh
