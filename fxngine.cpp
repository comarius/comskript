
#include <istream>
#include <algorithm>
#include <dirent.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <wait.h>
#include "fxngine.h"
#include "main.h"
#include "nutconf.h"
#include "globalfoos.h"
#include "bashoxe.h"

//--------------------------------------------------------------------------------------------
fxngine*            PFX;
int  fxngine::__lastactivity = 0;

void fxngine::chekin()
{
    fxngine::__lastactivity = time(0);
}

//--------------------------------------------------------------------------------------------
fxngine::fxngine()
{
    PFX = this;
}

//--------------------------------------------------------------------------------------------
fxngine::~fxngine()
{
    int st;

    this->signal_to_stop();
    P_INSTS = count_instances();
    if(P_INSTS>0)
    {
        GLOGI("Waiting for all spawned processes to exit" << P_INSTS);
        while(P_INSTS = count_instances() && _procque.size())
        {
            ::wait(&st);
            ::sleep(3);
        }
    }
    PFX = 0;
}

//--------------------------------------------------------------------------------------------
void fxngine::run(int nargs,  char* v[])
{
    if(nargs==3)
    {
        if(!::strstr(v[2],"://"))
        {
            Tempit t(v[2]);
            if(t.valid())
            {
//                sq_export_err(PFX->getsq_env().theVM());
//                sq_export(PFX->getsq_env().theVM());

                run_local(v[1], v[2]);
            }
            else
            {
                GLOGE("already running on a device id" << v[2] <<" If not delete tmp file" );
            }
            return;
        }
    }
    if(nargs==4)
    {
        Tempit t(v[3]);
        if(t.valid())
        {
//            sq_export_err(PFX->getsq_env().theVM());
//            sq_export(PFX->getsq_env().theVM());
            run_remote(v[1], v[2], v[3]);
        }
        else
        {
            GLOGE("already running on a device id" << v[3] <<" If not delete tmp file" );
        }
        return;
    }

    cout << v[0] << " test-file machineid                                  # to run script\n";
    cout << v[0] << " test-file ssh://<user>/<pass>@ip:port machineid      # to run script on remote\n";
    cout << v[0] << " test-file ooh://<user>/<pass>@ip:port machineid      # to run script on remote\n";
    cout << v[0] << " test-file tty:///dev/tty*:9600,n,8,1 machineid       # to run script on remote\n";
    cout << v[0] << " test-file adb://ANDROIDSERIAL:PORT machineid         # to run script on remote\n";
    cout << v[0] << " test-file adw://DEVICE_IP:ADB_PORT:DEV_PORT         # to run script on remote\n";
    cout << v[0] << " test-file usb://usbdevice machineid                  # to run script on remote\n";
    cout << v[0] << " test-file blu://btdevice machineid                  # to run script on remote\n";
    cout << v[0] << " test-file udp://ip:port machineid                    # to run script on remote\n";
}

//--------------------------------------------------------------------------------------------
void    fxngine::run_remote(const char* test, const char* cred, const char* mid)
{
    sq_run(cred, mid, _env, string(test));
}

//--------------------------------------------------------------------------------------------
void    fxngine::run_local(const char* test, const char* mid)
{
    sq_run(0, mid, _env, string(test));
}

//--------------------------------------------------------------------------------------------
string fxngine::_base_name(const char* test)
{
    string      sf = test;
    string      curscript;

    size_t      pslash = sf.string::find_last_of("/");
    size_t      pdot = sf.string::find_last_of(".");

    if(pdot!=string::npos)
    {
        if(pslash!=string::npos)
        {
            curscript=sf.substr(pslash+1, pdot-pslash-1);
        }
        else
        {
            curscript=sf.substr(0, pdot);
        }
    }
    else
    {
        curscript=sf;
    }
    return curscript;
}

//--------------------------------------------------------------------------------------------
void fxngine::sq_run(const char* cred, const char* mid, SqEnv& env, const string& sf)
{
    if(::access(sf.c_str(), 0)!=0)      // check if script file exists
    {
        GLOGE("program: " << sf << " was not found");
        return;
    }
    _deviceid = mid;
    if(cred && *cred)
        _maincred = cred;
    // this->_prepare_folders(env);
    sq_export(env.theVM());        // export functions and class methods to script
    sq_export_err(env.theVM());
    bashoxe::sq_export(env);
    Context::sq_export(env);
    try
    {
        GLOGI("program: " << sf << " Starts");
        MyScript s  = env.compile_script(sf.c_str());
        s.run_script();
        Sqrat::Function funcA(Sqrat::RootTable(env.theVM()), _SC("main"));
        if(!funcA.IsNull())
        {
            ScriptData  sd(&env, sf, _deviceid, 0);
            save_data(sd);
            funcA.Evaluate<int>(_maincred.c_str());
        }


        ScriptData sd = get_data();
        stringstream ofs;
        string step = sf;
        while(step.length()<50)step.append(" ");
        if(wasq_error())
        {
            ofs << str_time()<< " & "<< uint64_t(sd._defunct) <<" \t^\t"<< _sqerror <<"\t^\t"<<  c_states[CP_ERROR] << "\n";
        }
        else
        {
            ofs << str_time()<< " & "<< uint64_t(sd._defunct) <<" \t^\t"<< step <<"\t^\t"<<  c_states[CP_SUCCESS] << "\n";
        }
        ::fwriteline((sd._logpath + "state"), ofs.str());
        ::fwriteline((sd._logpath + "progress"), ofs.str(), true);

    }
    catch(ctxerr& err)
    {
        GLOGE(err.str());

        stringstream ofs;
        string step = err.str();

        while(step.length()<50)step.append(" ");
        ofs << str_time()<<" & "<< uint64_t(err.defunct()) <<"\t&\t"<< step <<"\t&\t"<<  c_states[CP_ERROR] << "\n";

        ::fwriteline(err.logfld("state"), ofs.str());
        ::fwriteline(err.logfld("progress"), ofs.str(), true);
    }
    catch(Sqrat::Exception ex)
    {
        GLOGE(ex.Message());
    }

    GLOGI("program: " << sf << " Ends");
}

//--------------------------------------------------------------------------------------------
size_t fxngine::_load_tests(vector<string>& tests)
{
    tests.push_back("test.tst");
    return tests.size();
}

//--------------------------------------------------------------------------------------------
bool   fxngine::run_inst(const char* s, const char* c, const char* i)
{
    P_INSTS = count_instances();
    if(P_INSTS > PNCONF->_globo.parralel)
    {
        GLOGD("task " << s << c << i << " was queued");
        AutoLock a(&_m);
        _procque.push_back(s);
        _procque.push_back(c);
        _procque.push_back(i);
        return true;
    }

    GLOGD("spawn " << s << c << i);
    char lococred[128]={0};

    if(c[0]=='@')
        ::strcpy(lococred, c+1);
    else
        ::strcpy(lococred, c);
    if(!PFX->credentials().empty() && c[0]!='@')
    {
        ::strcpy(lococred, PFX->credentials().c_str());
    }
    int fif = fork();
    if(fif==-1){
        GLOGE("FORK() -- CHILD PROC FOR :"  << s << " FAILED");
    }
    else if(fif==0)
    {
        GLOGE("FORK() --> "<<getpid()<<" CHILD PROC FOR :"  << s << " STARTED");
        do{
            SqEnv    env;
            sq_export_err(env.theVM());
            sq_export(env.theVM());
            PFX->sq_run(lococred, i, env, s);
        }while(0);
         GLOGD( "FORK() <-- --> "<<getpid()<<"  CHILD: " << P_INSTS << " EXITS");
        ::exit(0);
        ::msleep(100);
        P_INSTS = count_instances();
    }
    do{
        P_INSTS = count_instances();
        sleep(1);
    }while(P_INSTS > PNCONF->_globo.parralel && Inst.alive());
    return 1;
}


//--------------------------------------------------------------------------------------------
void fxngine::thread_main()
{
    while(!is_stopped() && Inst.alive())
    {
        stringstream    s; s << "/proc/"<<getpid()<<"/task";
        string  sl = ::freadline(s.str());
        P_INSTS = count_instances();
        if(P_INSTS < PNCONF->_globo.parralel)
        {
            AutoLock a(&_m);

            string s = _procque.front(); _procque.pop_front();
            string c = _procque.front(); _procque.pop_front();
            string i = _procque.front(); _procque.pop_front();
            GLOGD("task " << s << c << i << " was de-queued");
            run_inst(s.c_str(),c.c_str(),i.c_str());
        }
        ::sleep(1);


        if(time(0) - __lastactivity > (PNCONF->_globo.onlinewait * 5))
        {
            GLOGE("program found stuck for too long. Commiting suicide");
            signal_to_stop();
            Inst.kill();
        }
    }
}

//--------------------------------------------------------------------------------------------
uint32_t fxngine::count_instances()
{
    stringstream    ss; ss << "/proc/"<<P_ID<<"/task/"<<P_ID<<"/children";

    _children.clear();
    if(::access(ss.str().c_str(),0)==0)
    {
        AutoLock a(&_m);

        string   sl = ::freadline(ss.str());
        if(!sl.empty())
        {
            std::istringstream iss(sl);
            std::string token;

            while(getline(iss, token, ' '))
            {
                _children.push_back(token);
            }
        }
    }
    return _children.size();
}
