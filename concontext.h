#ifndef __CONTEXTX_H__
#define __CONTEXTX_H__

#include <string>
#include <os.h>
#include <sock.h>
#include "sqwrap.h"
#include <string>
#include <sslcrypt.h>
#include "remotetty.h"
#include "iothread.h"
#include "bashoxe.h"
#include "logger.h"
#include "testate.h"

#define SCRIPT

using namespace std;


class fxngine;
struct ScriptData;
class Context : public ttyevent
{
public:
    friend class            ctxloger;
    friend class            ctxerr;
    friend class            Progress;
public:
    Context();
    virtual                 ~Context();
//  exported
    static void           sq_export(SqEnv& env);
    SCRIPT const char*    login(const char* uph);
    SCRIPT bool           re_login();
    SCRIPT int            logout();
    SCRIPT bool            io_flush(){return io_flush(0);};
    SCRIPT bool            io_flush(int time);

    SCRIPT bool           monitor();

    SCRIPT bool           wr_ex(const char* cmd, const char* exp, int to);                   //ret expected true / false
    SCRIPT bool           wr_ex(const char* cmd, const char* exp){return wr_ex(cmd,exp,0);};
    SCRIPT bool           wr_ex(const char* cmd ){return wr_ex(cmd,_prompt.c_str(),0);};

    SCRIPT const char*    wr_rd(const char* cmd, int to);                   //ret expected true / false
    SCRIPT const char*    wr_rd(const char* cmd){return wr_rd(cmd,0);};

    SCRIPT bool           write(const char* cmd, const char* p, int to);                  // same
    SCRIPT bool           write(const char* cmd, const char* p){return write(cmd,p,0);}
    SCRIPT bool           write(const char* cmd){return write(cmd,0,0);}
    SCRIPT bool           reading();
    SCRIPT bool           is_ready();
    SCRIPT const char*    read(const char* cback, int to);
    SCRIPT const char*    read(const char* cback){return read(cback, 0);};
    SCRIPT const char*    read(){return read(0,0);};


    const char*             pick();
    SCRIPT bool            is_steady(int t);
    SCRIPT bool            is_steady(){return is_steady(0);};

    SCRIPT bool            expect(const char* regexpr, const char* cb, int to);
    SCRIPT bool            expect(const char* regexpr, const char* cb){return expect(regexpr,cb,0);};
    SCRIPT int             report(const char* s);
    SCRIPT bool            expect(const char* cb, int to){return expect(_prompt.c_str(),cb,to);};
    SCRIPT bool            expect(const char* cb){return expect(_prompt.c_str(),cb,0);};
    SCRIPT bool            expect(){return expect(_prompt.c_str(),0,0);};


    SCRIPT int            log_status(int state, const char* log);
    SCRIPT bool            send_key(const char* code);
    SCRIPT int            sleep(int t);
    SCRIPT int            io_clear();
    SCRIPT int            send_passwd();
    SCRIPT int            exito(const char* message);
    SCRIPT int            set_timout(int secs);
    SCRIPT const char*    get_input(const char* prompt); // 0 from stdio, otherwise from /input/host/script.in  file
    SCRIPT bool            put_file(const char* local, const char* remote);
    SCRIPT bool            get_file(const char* remote, const char* local);
    SCRIPT const char*    get_prompt();
    SCRIPT const char*    get_uid();
    SCRIPT int            pause(bool pause);
    SCRIPT bool           show_term(const char* constring);
    SCRIPT bool           download(const char* remote);
    SCRIPT const char*    mlwrite(const char* mline, const char* cb, int to);
    SCRIPT const char*    mlwrite(const char* mline, const char* cb){return mlwrite(mline,cb,0);};
    SCRIPT const char*    mlwrite(const char* mline){return mlwrite(mline,0,0);};

    const string&           script()const{return _script;}
    const string&           host()const{return _host;}
    const string&           uuid()const{return _deviceid;}
    const string&           basefolder()const{return _logpath;}
    bool                    intime(time_t fut);
    void                    tty_event(eNUMEXP code);
    void                    log_prog(C_PROGRESS state, const stringstream& ctnt);

protected:
    bool            _dump_file(const string& file, const char* cb);
    void             _is_paused();
    void            _initialize(const string& prooto, const string& user, const string& pass, const string& host, int port, const string& args);
    void            _tty_run();
    void            _sq_run();
    void            _wait_tosend();
    bool            _is_private(const string&);
    void            _parse_authstr(const string& uph, string& proto, string& user,string& pass,string& host,size_t& port,string& args);
    void            _init_logs(const ScriptData& sd);
    void            _check_log_size();
    bool            _notify_script(const char* funcname, const char* line, const char* el=0);
    bool            _store_output(const string& o);
    bool            _read_output(string& ret);
    bool            _flushoutput(const char* caller,int to, bool noty=false);
    int             _keep_reading(int to, const char* cb);
    string          _fcin()const{return         _logpath+"cin";}
    string          _fcout()const{return        _logpath+"cout";}
    string          _fifofout()const{return        _logpath+"fifoout";}
    string          _finout()const{return        _logpath+"inout";}
    string          _fstatus()const{return      _logpath+"state";}
    string          _fprogress()const{return    _logpath+"progress";}
    string          _ftemp()const{return        _logpath+"temp";}
    string          _fproto()const{return        _logpath+"proto";}
    string          _fhost()const{return        _logpath+"host";}
    string          _fuser()const{return        _logpath+"user";}
    string          _finter()const{return        _logpath+"inter";}
    string          _flogs()const{return        _logpath+"logs";}
    string          _fuid()const{return         _logpath+"uid";}
    string          _fuip()const{return         _logpath+"host";}
    string          _fscript()const{return      _logpath+"script";}
    string          _fthis()const{return        _logpath+"this";}
    string          _fpid()const{return        _logpath+"pid";}
    string          _fbreak()const{return      _logpath+"break";}
    string          _fstack()const{return      _logpath+"stack";}
    string          _fpause()const{return      _logpath+"pause";}
    string          _fbackup()const{return     _logpath+"backup";}
    string          _freport()const{return     _logpath+"REPORT";}
    string          _ferror()const{return     _logpath+"error";}
    void            _mon(const char* dir, const string& data);
    string          _fsystemp()const{
        string tmp="/tmp/ftctx-"; tmp += std::to_string(getpid());
        return tmp;
    }
    void            _backup(const string & file);
    bool            _to_shell(const string& s);
    bool            _from_shell(string& s);
    string          _str_login(const char* uph);
protected:
    mutex           _m;
    IoThread        _iopump;
    string          _fromshell;
    string          _ret;
    string          _error;
    int             _tout;
    string          _protocol;
    string          _user;
    string          _pass;
    string          _host;
    string          _args;
    string          _prompt;
    string          _tmplog;
    size_t          _port;
    string          _script;
    size_t          _readoff;
    SqEnv*          _psqenv;
    int             _blog;
    string          _logpath;
    time_t          _fut;
    bool            _ttyonline;
    string          _credentials;
    string          _deviceid;
    static int      _cindex;
    C_PROGRESS      _exit_state;
    string          _exit_msg;
    int             _outfifo;
    udp_p2p         _mont;
    string          _lastcmd_2_shell;  //to find out the echo
};



#endif //__CONTEXT_H__
