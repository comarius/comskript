#ifndef NUTCONF_H
#define NUTCONF_H

#include <config.h>

class nutconf : public Conf
{
public:
     nutconf(const char* fname);
    virtual ~nutconf();

    struct protocol{
        string  prestart;
        string  initstring;
        string  exitstring;
        string  escape;
        int     port;
        int     devport;
        string  param;
        map<string, protocol> sett;
    };
    void _assign( const char* pred, const char* val, int line);
    void _subassign( const char* pred, const char* val, int line);
    virtual bool finalize();
protected:
    void _escapes(protocol& proto);
    void  _load_dev(protocol& proto, const char* file);

private:

public:


    struct struct_tests
    {
        struct_tests():cmdtime(5),retrytime(10),testbase("tests"),cmdwait(3),checkid(0),
                        resolveuid(0),inputwait(1200),outlength(1000),monitor(0),distinct(0)
        {

        }
        //int     port;

        int     onlinewait;
        int     parralel;
        int     cmdtime;
        int     retrytime;
        string  trace;
        string  terminal;
        string  testbase;
        int     cmdwait;
        int     checkid;
        int     resolveuid;
        int     inputwait;
        int     outlength;

        string  user;
        int     monitor;
        int     distinct;
    }_globo;

    struct suite
    {
        string  targets;
        string  tests;
        string  binary;
        string  temp;
        string  escape;

    }_suite;


    protocol  _ssh;
    protocol  _ooh;
    protocol  _tty;
    protocol  _udp;
    protocol  _adb;
    protocol  _usb;
    protocol*  _pselected;

};
extern nutconf* PNCONF;
#endif // NUTCONF_H
