


#include "nutconf.h"

//-----------------------------------------------------------------------------
#define BIND(mem_,name_)     _bind(lpred, #name_,  mem_.name_, val);

nutconf* PNCONF;

nutconf::nutconf(const char* fname):_pselected(0)
{
    PNCONF = this;
    this->load(fname, (PAS)&nutconf::_assign);

    _adb.exitstring="0009host:kill";
}

nutconf::~nutconf()
{
    PNCONF = 0;
}

void nutconf::_assign( const char* pred, const char* val, int line)
{
    char    lpred[256];
    char    loco[256];

   // ::strcpy(loco,_section.c_str());
    ::strcpy(loco,val);
    ::strcpy(lpred,pred);

    if(_section=="[global]")
    {
        BIND(_globo,user);
        BIND(_globo,testbase);
        BIND(_globo,onlinewait);
        BIND(_globo,parralel);
        BIND(_globo,cmdtime);
        BIND(_globo,retrytime);
        BIND(_globo,trace);
        BIND(_globo,terminal);
        BIND(_globo,cmdwait);
        BIND(_globo,checkid);
        BIND(_globo,resolveuid);
        BIND(_globo,outlength);
        BIND(_globo,distinct);
        BIND(_globo,inputwait);
        BIND(_globo,monitor);
    }
    else if(_section=="[suite]")
    {
        BIND(_suite,targets);   // folder where targets are manages stored
        BIND(_suite,tests);     // forder where tests are stored
        BIND(_suite,binary);
        BIND(_suite,temp);
        BIND(_suite,escape);

    }
    else if(_section=="[ssh]")
    {
        BIND(_ssh,initstring);   // folder where targets are manages stored
        BIND(_ssh,exitstring);     // forder where tests are stored
        BIND(_ssh,prestart);
        BIND(_ssh,escape);
        BIND(_ssh,param);
    }
    else if(_section=="[ooh]")
    {
        BIND(_ooh,initstring);   // folder where targets are manages stored
        BIND(_ooh,exitstring);     // forder where tests are stored
        BIND(_ooh,prestart);
        BIND(_ooh,escape);
        BIND(_ooh,param);
    }
    else if(_section=="[tty]")
    {
        BIND(_tty,initstring);   // folder where targets are manages stored
        BIND(_tty,exitstring);     // forder where tests are stored
        BIND(_tty,prestart);
        BIND(_tty,escape);
        BIND(_tty,param);
        if(!strcmp(lpred,"url"))
        {
            _load_dev(_tty, val);
            cout << _tty.sett.size() << "\n";
        }
    }
    else if(_section=="[udp]")
    {
        BIND(_udp,initstring);   // folder where targets are manages stored
        BIND(_udp,exitstring);     // forder where tests are stored
        BIND(_udp,prestart);
        BIND(_udp,escape);
        BIND(_adb,param);
    }
    else if(_section=="[usb]")
    {
        BIND(_usb,initstring);   // folder where targets are manages stored
        BIND(_usb,exitstring);     // forder where tests are stored
        BIND(_usb,prestart);
        BIND(_usb,escape);
        BIND(_usb,param);
    }
    else if(_section=="[adb]")
    {
        BIND(_adb,initstring);   // folder where targets are manages stored
        BIND(_adb,exitstring);     // forder where tests are stored
        BIND(_adb,prestart);
        BIND(_adb,escape);
        BIND(_adb,param);
        BIND(_adb,port);
        BIND(_adb,devport);
    }

}

bool nutconf::finalize()
{
    _blog=0;

    _blog |= _globo.trace.find('I') == string::npos ? 0 : 0x1;
    _blog |= _globo.trace.find('W') == string::npos ? 0 : 0x2;
    _blog |= _globo.trace.find('E') == string::npos ? 0 : 0x4;
    _blog |= _globo.trace.find('D') == string::npos ? 0 : 0x8;
    _blog |= _globo.trace.find('T') == string::npos ? 0 : 0x10;
    _blog |= _globo.trace.find('X') == string::npos ? 0 : 0x20;
    _blog |= _globo.trace.find('H') == string::npos ? 0 : 0x40;
    _blog |= _globo.trace.find('A') == string::npos ? 0 : 0xFF;

    string syscmd = "mkdir  -p ";
    syscmd += _globo.testbase;
    ::system(syscmd.c_str());
    _logs_path = _globo.testbase;

    _escapes(_ssh);
    _escapes(_ooh);
    _escapes(_tty);
    _escapes(_udp);
    _escapes(_adb);
    _escapes(_usb);

    string user;
    if(!_globo.user.empty())
        user=_globo.user;
    else
    {
        user = getenv("USER");
    }
    stdstrreplace(_globo.testbase,"$USER", user);
    stdstrreplace(_suite.binary,"$USER", user);
    stdstrreplace(_suite.temp,"$USER", user);
    stdstrreplace(_suite.targets,"$USER", user);

    return Conf::finalize();
}

void nutconf::_escapes(protocol& proto)
{
    size_t esclen = proto.escape.length();
    if(esclen>=2)
    {
        string& esc = proto.escape;
        size_t pesc = esc.find("^");
        while(pesc != string::npos)
        {
            char code = esc[pesc+1]-'A'+1;
            esc.replace(pesc,2,1,code);
            pesc = esc.find("^");
        }
        pesc = esc.find("0x");
        while(pesc != string::npos)
        {
            char seq=(char)(std::stoul(esc.substr(pesc+2,2), nullptr, 16));
            esc.replace(pesc,4,1,seq);
            pesc = esc.find("0x");
        }
    }
}


void  nutconf::_load_dev(protocol& proto, const char* file)
{
    _pselected=&proto;
    this->load(file, (PAS)&nutconf::_subassign);
}

void nutconf::_subassign( const char* pred, const char* val, int line)
{
    char    lpred[256];
    char    loco[256];

   // ::strcpy(loco,_section.c_str());
    ::strcpy(loco,val);
    ::strcpy(lpred,pred);

     if(_section[0]=='[')
     {
        string secname=_section.substr(1,_section.length()-2);
        if(_pselected->sett.find(secname)==_pselected->sett.end())
        {
            protocol proto;
           _pselected->sett[secname]=proto;
        }

        BIND(_pselected->sett[secname],initstring);   // folder where targets are manages stored
        BIND(_pselected->sett[secname],exitstring);     // forder where tests are stored
        BIND(_pselected->sett[secname],prestart);
        BIND(_pselected->sett[secname],escape);
        BIND(_pselected->sett[secname],param);
     }
}







