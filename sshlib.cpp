/*
    Author: Marius Octavian Chincisan, Jan-Aug 2012
    Copyright: Marius C.O.
*/
#include <assert.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/stat.h>
#include <utime.h>
#include <pthread.h>
#include <limits.h>
#include "nutconf.h"
#include "strutils.h"
#include "globalfoos.h"
#include "sqmod.h"
#include "sshlib.h"



eNUMEXP SSh::_verify_knownhost()
{
    size_t          state=0, hlen=0;
    uint8_t   *hash = 0;
    char            *hexa = 0;
    ssh_key         srv_pubkey = 0;

    state = ssh_is_server_known(_ssh_session);
    hlen = ssh_get_pubkey_hash(_ssh_session, &hash);
/*
this crashes when opening multiple ssh sessions
ssh_get_publickey_hash(srv_pubkey,
                        SSH_PUBLICKEY_HASH_SHA1,
                        &hash,
                        &hlen);
*/
    if ((int)hlen <= 0)
        return EX_AUTH;
    switch (state)
    {
    case SSH_SERVER_KNOWN_OK:
        break; /* ok */
    case SSH_SERVER_KNOWN_CHANGED:
        GLOGE("Host key for server changed: it is now");
        GLOGE("Public key hash" << hash << hlen);
        GLOGE("For security reasons, connection will be stopped");
        free(hash);
        return EX_AUTH;
    case SSH_SERVER_FOUND_OTHER:
        GLOGE("The host key for this server was not found but an other"
                "type of key exists.");
        GLOGE("An attacker might change the default server key to"
                "confuse your client into thinking the key does not exist");
        free(hash);
        return EX_AUTH;
    case SSH_SERVER_FILE_NOT_FOUND:
        GLOGE("Could not find known host file.");
        GLOGE("If you accept the host key here, the file will be"
                "automatically created.");
    /* fallback to SSH_SERVER_NOT_KNOWN behavior */
    case SSH_SERVER_NOT_KNOWN:
        hexa = ssh_get_hexa(hash, hlen);
        GLOGI("The server is unknown. Do you trust the host key?");
        GLOGI( "Public key hash: " << hexa);
        free(hexa);
        /*
        if (fgets(buf, sizeof(buf), stdin) == NULL)
        {
            free(hash);
            return -1;
        }
        if (strncasecmp(buf, "yes", 3) != 0)
        {
            free(hash);
            return -1;
        }
        */

        if (ssh_write_knownhost(_ssh_session) < 0)
        {
            GLOGE("Error " << strerror(errno));
            free(hash);
            return EX_AUTH;
        }
        break;
    case SSH_SERVER_ERROR:
        GLOGE("Error" << ssh_get_error(_ssh_session));
        free(hash);
        return EX_AUTH;
    }
    free(hash);
    return EX_NONE;
}

SSh::SSh():_ssh_session(0),_channel(0)
{
}


SSh::~SSh()
{
    close();
    sleep(1);
}

eNUMEXP SSh::connect()
{
    return connect(_user.c_str(), _pass.c_str(), _host.c_str(), _port, _args);
}

eNUMEXP SSh::connect(const char* user, const char* pass, const char* host, int port, const string& args)
{
    _user = user;
    _pass = pass;
    _host = host;
    _port = port;
    _args= args;


    _ssh_session=ssh_new();
    if(0==_ssh_session)
    {
        return EX_CONNECT;
    }
    int verbosity = 0;//SSH_LOG_PROTOCOL;
    ssh_options_set(_ssh_session, SSH_OPTIONS_HOST, host);
    ssh_options_set(_ssh_session, SSH_OPTIONS_LOG_VERBOSITY, &verbosity);
    ssh_options_set(_ssh_session, SSH_OPTIONS_PORT, &port);
    ssh_options_set(_ssh_session, SSH_OPTIONS_USER, user);

    int rc = ssh_connect(_ssh_session);
    if (rc != SSH_OK)
    {
        GLOGE("Error connecting to <<" << host << " " << ssh_get_error(_ssh_session));
        return EX_CONNECT;
    }
    eNUMEXP cr = _verify_knownhost();
    if (cr != EX_NONE)
    {
        return cr;
    }
    rc = ssh_userauth_password(_ssh_session, NULL, pass);
    if (rc != SSH_AUTH_SUCCESS)
    {
        return EX_AUTH;
    }

    return EX_NONE;
}

eNUMEXP SSh::open_tty()
{
    assert(_ssh_session);

    _channel = channel_new(_ssh_session);
    if (_channel == NULL)
    {
        GLOGE("Error creating ssh channel"  << ssh_get_error(_ssh_session));
        return EX_RESOURCE;
    }
    int rc = channel_open_session(_channel);
    if (rc != SSH_OK)
    {
        GLOGE("Error openning ssh session"  << ssh_get_error(_ssh_session));
        return EX_RESOURCE;
    }

    rc = channel_request_pty(_channel);
    if (rc != SSH_OK)
        return EX_OPENTTY;
    rc = channel_change_pty_size(_channel, 80, 24);
    if (rc != SSH_OK)
        return EX_OPENTTY;
    rc = channel_request_shell(_channel);
    if (rc != SSH_OK)
        return EX_OPENTTY;

    if((channel_is_open(_channel) && !channel_is_eof(_channel)))// ? EX_NONE : EX_OPENTTY;
    {
        if(EX_NONE == _open_tty(PNCONF->_ssh))
        {
            _transact("\r",_prompt);
            GLOGI("prompt is: " << _prompt);
            return EX_NONE;
        }
    }
    return EX_OPENTTY;
}

eNUMEXP SSh::receive(string& s)
{
    assert(_channel);

    s.clear();
    char chunk[4096] = {0};
    int nbytes = channel_read_nonblocking(_channel, chunk, sizeof(chunk)-32, 0);
    if (nbytes <= 0)
    {
        const char* sslerr = ssh_get_error(_ssh_session);
        if(channel_is_eof(_channel))
        {
            GLOGE(sslerr << "," << errno);
            return EX_CONBROKEN;
        }
    }
    if (nbytes > 0)
    {
        chunk[nbytes]= 0;
        s.append(chunk);
        return EX_NONE;
    }
    return EX_POOLING;
}

eNUMEXP SSh::send(const string& c)
{
    assert(_channel);
    size_t bytes = c.length();
    size_t sent = 0;
    while(bytes > 0)
    {
        int nwritten = channel_write(_channel, c.substr(sent).c_str(), bytes);
        if(nwritten>0)
        {
            bytes-=nwritten;
            sent+=nwritten;
        }
        else
        {
            GLOGE("ssh Error to send");
            break;
        }
    }
    return bytes == 0 ? EX_NONE : EX_CONBROKEN;
}

bool SSh::isopen()
{
    return (_channel && channel_is_open(_channel));// && !channel_is_eof(_channel));
}

void SSh::close()
{
    if(_channel==0)
        return;
    if(!PNCONF->_ssh.exitstring.empty())
        send(PNCONF->_ooh.exitstring);
    else{
        GLOGE("there is not  exit string on ssh. using exit");
        send(string("exit\r\n"));
    }

    if(_channel)
    {
        channel_send_eof(_channel);
        sleep(3);
        channel_close(_channel);
        channel_free(_channel);
        _channel= 0;
    }

    if(_ssh_session)
    {
        ssh_disconnect(_ssh_session);
        ssh_free(_ssh_session);
        _ssh_session=0;
    }
}


eNUMEXP SSh::put_file(const char* local, const char* remote)
{
    GLOGT(__FUNCTION__ << local <<","<<remote);
    assert(_ssh_session);
    sftp_session sftp=sftp_new(_ssh_session);
    if(!sftp)
    {
        GLOGE("cannot open sftp_session" <<  ssh_get_error(_ssh_session));
        return EX_SSH;
    }
    AutoCall<void (*)(sftp_session), sftp_session> ac(::sftp_free, sftp);

    if(sftp_init(sftp))
    {
        GLOGE("error initialising sftp:" <<  ssh_get_error(_ssh_session));
        return EX_SSH;
    }
    sftp_file fremote = sftp_open(sftp,remote,O_WRONLY|O_CREAT, 0644);

    if(!fremote)
    {
        fprintf(stderr, "Error opening %s: %s\n", remote , ssh_get_error(_ssh_session));
        return EX_SSH;
    }

    AutoCall<int (*)(sftp_file), sftp_file>  acc(::sftp_close, fremote);

    FILE* pl = ::fopen(local,"rb");
    if(0==pl)
    {
        GLOGE("cannot open: " <<  local);
        return EX_RESOURCE;
    }
    AutoCall<int (*) (FILE *),FILE*>    accc(::fclose, pl);
    char        buffer[4096];
    int         bytes;
    while(!feof(pl))
    {
        bytes = ::fread(buffer,1,sizeof(buffer),pl);
        if(bytes>0)
        {
             bytes=::sftp_write(fremote,buffer,bytes);
             //GLOGT("sftp-writing: " << fremote <<","<< bytes << " bytes");
        }
        if(::feof(pl) || bytes <= 0)
            break;
        ::msleep(100);
    }
    GLOGT(__FUNCTION__ << remote <<","<<local << "DONE");
    return EX_NONE;
}


eNUMEXP SSh::get_file(const char* remote, const char* local)
{
    GLOGT(__FUNCTION__ << remote <<","<<local);
    assert(_ssh_session);
    sftp_session sftp=sftp_new(_ssh_session);
    if(!sftp)
    {
        GLOGE("cannot open sftp_session" <<  ssh_get_error(_ssh_session));
        return EX_SSH;
    }
    AutoCall<void (*)(sftp_session), sftp_session> ac(::sftp_free, sftp);

    if(sftp_init(sftp))
    {
        GLOGE("error initialising sftp:" <<  ssh_get_error(_ssh_session));
        return EX_SSH;
    }

    sftp_file fremote = sftp_open(sftp,remote,O_RDONLY,0);
    if(!fremote)
    {
        fprintf(stderr, "Error opening %s: %s\n", remote , ssh_get_error(_ssh_session));
        return EX_SSH;
    }

    AutoCall<int (*)(sftp_file), sftp_file>  acc(::sftp_close, fremote);

    FILE* pl = fopen(local,"wb");
    if(0==pl)
    {
        GLOGE("cannot open: " <<  local);
        return EX_RESOURCE;
    }
    AutoCall<int (*) (FILE *),FILE*>    accc(fclose, pl);
    char                                buffer[4096];
    int                                 bytes,written;

    while((bytes=sftp_read(fremote,buffer,sizeof(buffer))) > 0)
    {
        written = ::fwrite(buffer,1,bytes,pl);
        //GLOGT("writing: " << fremote <<","<< written << " bytes");
        if(bytes != written)
        {
             GLOGE("error writing to:" << local);
             return EX_CONBROKEN;
        }
    }
     GLOGT(__FUNCTION__ << remote <<","<<local << "DONE");
    return EX_NONE;
}


