#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <sys/termios.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>
#include <fcntl.h>
#include <iostream>
#include <string>
#include <stdio.h>
#include "nutconf.h"
#include "devtty.h"


devtty::devtty():_fd(0),_speed(115200),_databits(8),_stopbits(1),_parity("n")
{

}

devtty::~devtty()
{
    close();
}

/**

    /dev/asdfa:115200,n,8,1
*/
eNUMEXP devtty::connect(const char* user, const char* pass, const char* host, int port, const string& args)
{
    string  temp;
    int     slot = 0;

    _user = user;
    _pass = pass;
    _host = host;
    _port = port;
    _args= args;

    _device.clear();
    for(uint32_t i=0; i < ::strlen(host); i++)
    {
        char c = host[i];
        if(c==':' && _device.empty())
        {

            _device= temp;
            slot++;
            temp.clear();
            continue;
        }
        if(c==',')
        {
            if(slot==1)
                _speed=::atoi(temp.c_str());
            if(slot==2)
                _parity=temp;
            if(slot==3)
                _databits = ::atoi(temp.c_str());
            if(slot==4)
                _stopbits = ::atoi(temp.c_str());
            slot++;
            temp.clear();
            continue;
        }
        temp+=c;
    }

    if(_databits<5 || _databits>8 || _stopbits <=0 || _stopbits > 2)
    {
        return EX_OPENTTY;
    }
    // /dev/ttyusb0 115200:n:8:1 as host
    assert(_fd==0);
    _fd=open(_device.c_str(), O_RDWR | O_NDELAY);
    if (_fd < 0)
    {
        return EX_CONNECT;
    }
    ::tcflush(_fd, TCIOFLUSH);
    int n = ::fcntl(_fd, F_GETFL, 0);
    fcntl(_fd, F_SETFL, n & ~O_NDELAY);
    if(_set_options())
    {
        struct termios newtio = {0};
        if (::tcgetattr(_fd, &newtio)!=0)
        {
            GLOGE(">tcgetattr() failed\n");
            return EX_OPENTTY;
        }
        newtio.c_lflag &= ~ICANON;
        if (::tcsetattr(_fd, TCSANOW, &newtio)!=0)
        {
            GLOGE(">tcgetattr() failed\n");
            return EX_OPENTTY;
        }
        return EX_NONE;
    }
    return EX_OPENTTY;
}


eNUMEXP devtty::connect()
{
    return connect(_user.c_str(), _pass.c_str(), _host.c_str(), _port, _args);
}

eNUMEXP devtty::open_tty()
{
    eNUMEXP ex = EX_NONE;
    //see if we have special port settings.
    if(PNCONF->_tty.sett.find(_device)!=PNCONF->_tty.sett.end())
    {
        ex = _open_tty(PNCONF->_tty.sett[_device]);
        if(ex == EX_NONE && !PNCONF->_tty.sett[_device].initstring.empty())
        {
            _transact("\r",_prompt);
            GLOGT("prompt is: " << _prompt);
        }
    }
    else
    {
        ex = _open_tty(PNCONF->_tty);
        if(ex == EX_NONE)
        {
            _transact("\r",_prompt);
            GLOGT("prompt is: " << _prompt);
        }
    }
    return ex;
}

eNUMEXP devtty::receive(string& s)
{
    if(_fd<=0)  return EX_CONBROKEN;
    struct      timeval set_timout = {0,100000};
    fd_set      rset;
    int         ndescs = _fd+1;
    int         tries = 8;
    eNUMEXP     ex;

AGAIN:
    FD_SET (_fd, &rset);
    set_timout.tv_usec = 100000;
    set_timout.tv_sec=0;
    int rv = select (ndescs, &rset, 0, NULL, &set_timout);
    if(rv < 0 && tries-->0)
    {
        ::msleep(100);
        if(tries<3)
        {
            close();
            ex = _open_tty(PNCONF->_tty);
            if(ex != EX_NONE)
            {
                goto AGAIN;
            }
        }
        else
        {
            goto AGAIN;
        }
        GLOGE("device subsystem error" << errno);
        sleep(1);
        return EX_NETWORK;
    }
    if(rv > 0)
    {
        char shot[256];
        int bytes = ::read(_fd, shot, sizeof(shot)-1);
        if(bytes == 0)
        {
            this->close();
            GLOGE("device down" << errno);
            return EX_CONBROKEN;
        }
        if(bytes > 0 )
        {
            shot[bytes]=0;
            s = shot;
            GLOGT("\n[received]\n" << s);
            return EX_NONE;
        }
    }
    ::msleep(50);
    return EX_POOLING;
}

eNUMEXP devtty::send(const string& c)
{
/*
    char loco[2]= {0};
    for(auto ch : c)
    {
        loco[0] = ch;
        if(1 != write(_fd, loco, 1))
        {
            return EX_CONBROKEN;
        }
        ::msleep(10);
    }
    GLOGT("\n[sent]\n" << c);
    return EX_NONE;
    */
    if(c.length()!=::write(_fd, c.c_str(), c.length()))
        return EX_CONBROKEN;
    return EX_NONE;
}

eNUMEXP devtty::put_file(const char* src, const char* dst)
{
    return EX_RESOURCE;
}

eNUMEXP devtty::get_file(const char* src, const char* dst)
{
    return EX_RESOURCE;
}


bool     devtty::isopen()
{
    return _fd>0;
}

void     devtty::close()
{
    if(_fd>0)
    {
        if(!PNCONF->_tty.exitstring.empty())
            send(PNCONF->_ooh.exitstring);
        else{
            GLOGE("there is not  exit string on adbtty. using exit");
            send(string("exit\r\n"));
        }
        ::close(_fd);
    }
    _fd=0;
}

bool devtty::_set_options()
{
    struct termios newtio = {0};

//   memset(&newtio, 0, sizeof(newtio));
    if (::tcgetattr(_fd, &newtio)!=0)
    {
        std::cerr<<"tcgetattr() 3 failed"<<std::endl;
    }

    cfsetospeed(&newtio, (speed_t)_speed);
    cfsetispeed(&newtio, (speed_t)_speed);


    switch (_databits)
    {
    case 5:
        newtio.c_cflag = (newtio.c_cflag & ~CSIZE) | CS5;
        break;
    case 6:
        newtio.c_cflag = (newtio.c_cflag & ~CSIZE) | CS6;
        break;
    case 7:
        newtio.c_cflag = (newtio.c_cflag & ~CSIZE) | CS7;
        break;
    case 8:
    default:
        newtio.c_cflag = (newtio.c_cflag & ~CSIZE) | CS8;
        break;
    }
    newtio.c_cflag |= CLOCAL | CREAD;
    //parity
    newtio.c_cflag &= ~(PARENB | PARODD);
    if (_parity =="e")
    {
        newtio.c_cflag |= PARENB;
    }
    else if (_parity == "o")
    {
        newtio.c_cflag |= (PARENB | PARODD);
    }
    newtio.c_cflag &= ~CRTSCTS;

    //stopbits
    if (_stopbits==2)
    {
        newtio.c_cflag |= CSTOPB;
    }
    else
    {
        newtio.c_cflag &= ~CSTOPB;
    }
    newtio.c_iflag=IGNBRK;

    if (0) //softwareHandshake)
    {
        newtio.c_iflag |= IXON | IXOFF;
    }
    else
    {
        newtio.c_iflag &= ~(IXON|IXOFF|IXANY);
    }

    newtio.c_lflag=0;
    newtio.c_oflag=0;

    newtio.c_cc[VTIME]=1;
    newtio.c_cc[VMIN]=60;

//   tcflush(m_fd, TCIFLUSH);
    if (tcsetattr(_fd, TCSANOW, &newtio)!=0)
    {
        std::cerr<<"tcsetattr() 1 failed"<<std::endl;
    }

    int mcs=0;
    ::ioctl(_fd, TIOCMGET, &mcs);
    mcs |= TIOCM_RTS;
    ::ioctl(_fd, TIOCMSET, &mcs);

    if (tcgetattr(_fd, &newtio)!=0)
    {
        GLOGE("tcgetattr()  failed");
        return false;
    }
    /*
    //hardware handshake
    if (hardwareHandshake)
    {
    newtio.c_cflag |= CRTSCTS;
    }
    else
    {
    newtio.c_cflag &= ~CRTSCTS;
    }
    if (on)
    newtio.c_cflag |= CRTSCTS;
    else
    newtio.c_cflag &= ~CRTSCTS;
    */
    if (::tcsetattr(_fd, TCSANOW, &newtio)!=0)
    {
        GLOGE("tcsetattr()  failed");
        return false;
    }
    return true;
}


