#ifndef UDPERLIST_H
#define UDPERLIST_H

#include <os.h>
#include <map>

using namespace std;

class udperlist : public OsThread
{
public:
    udperlist();
    virtual ~udperlist();

    void automatic();
protected:
private:
    map<string, string > _tqueue;

};

#endif // UDPERLIST_H
