#ifndef EXTERM_H
#define EXTERM_H


#include <string>
#include "testate.h"
#include "iothread.h"

using namespace std;


class exterm : public ttyevent
{
public:
    exterm();
    virtual ~exterm();
    eNUMEXP iotty(const string& proto,
                    const string& us,
                     const string& pa,
                     const string& ho,
                     size_t po,
                     const string& arg,
                     const string& did);
    void tty_event(eNUMEXP code);
protected:
private:
    string      _prompt;
    string      _retval;
    IoThread    _iopump;

};





#endif // EXTERM_H
