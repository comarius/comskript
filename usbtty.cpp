#include "usbtty.h"

usbtty::usbtty()
{
    //ctor
}

usbtty::~usbtty()
{
    //dtor
}

eNUMEXP usbtty::connect(const char* user, const char* pass, const char* host, int port, const string& args)
{
    _user = user;
    _pass = pass;
    _host = host;
    _port = port;
    _args= args;

    return EX_CONNECT;
}

eNUMEXP usbtty::connect()
{
    return EX_CONNECT;
}

eNUMEXP usbtty::open_tty()
{
    return _open_tty(PNCONF->_usb);
}

eNUMEXP usbtty::receive(string& s)
{
    return EX_CONNECT;
}

eNUMEXP usbtty::send(const string& c)
{
    return EX_CONNECT;
}

eNUMEXP usbtty::put_file(const char* src, const char* dst)
{
    return EX_CONNECT;
}

eNUMEXP usbtty::get_file(const char* src, const char* dst)
{
    return EX_CONNECT;
}

bool    usbtty::isopen()
{
    return false;
}

void    usbtty::close()
{
    return ;
}

