#ifndef FILEXTERM_H
#define FILEXTERM_H


#include <string>
#include "testate.h"
#include "iothread.h"

using namespace std;



class fileterm : public ttyevent
{
public:
    fileterm();
    virtual ~fileterm();
    eNUMEXP iotty(const string& pr,
                    const string& us,
                     const string& pa,
                     const string& ho,
                     size_t po,
                     const string& arg,
                     const string& did);
    void tty_event(eNUMEXP code);
protected:
private:
    IoThread    _iopump;
};

#endif // EXTERM_H
