#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <fcntl.h>
#include <stdio.h>
#include <algorithm>
#include "main.h"
#include "nutconf.h"
#include "sockimpl.h"
#include "exterm.h"
#include "fxngine.h"

using namespace std;

exterm::exterm():_iopump(this)
{
}

exterm::~exterm()
{
    _iopump.stop_thread();
}


void exterm::tty_event(eNUMEXP code)
{
    GLOGI("event from tty");
}

eNUMEXP exterm::iotty(const string& proto,
                      const string& us,
                      const string& pa,
                      const string& ho,
                      size_t po,
                      const string& arg,
                      const string& did)
{
    string  s;
    string  name=did;
    string  basename("/tmp/SH_"); basename += did;
    string  srv(basename);
    string  cli(basename);
    udp_p2p u;

    _iopump.clear();
    _iopump.reinit();
    eNUMEXP err = _iopump.connect_tty(proto, us, pa, ho, po, arg);
    if(err!=EX_NONE)
    {
        GLOGE("Could not connect");
        return EX_CONNECT;
    }
    _prompt = _iopump.get_prompt();
    _iopump.start_thread();
    srv += "_S";
    cli += "_C";

    if(u.create(srv.c_str(), cli.c_str()))
    {
        string          sfrom;
        int             sel;
        size_t          bytes;
        int             nfd = u.sock()+1;                // select() return value
        fd_set          rfd;        // file descriptor read flags
        struct timeval  tv;
        char            io[256];

        string exec="./ftcio "; exec+="\""; exec+=basename; exec+="\" "; exec +="&";
        ::sprintf(io,PNCONF->_globo.terminal.c_str(), exec.c_str());
        system(io);
        while(Inst.alive())
        {
            fxngine::chekin();
            FD_ZERO(&rfd);
            FD_SET(u.sock(), &rfd);  // add pipe to the read descriptor watch list
            FD_SET(0, &rfd);  // add pipe to the read descriptor watch list        tv.tv_sec = 0;
            tv.tv_usec = 10000;
            tv.tv_sec=0;
            sel = select(nfd, &rfd, NULL, NULL, &tv);
            if(sel==0)
            {
                sfrom.clear();
                if(_iopump.from_shell(sfrom))
                {
                    if(sfrom.length()==1 && sfrom[0]==0x3)
                    {
                        GLOGE("Escape or Break key detected. Ending interractive session");
                        break;
                    }

                    bytes = u.send(sfrom.c_str(), sfrom.length());
                    cout << sfrom;
                    if(bytes!=sfrom.length())
                    {
                        GLOGE("sendto " << cli << " error:" << errno);
                    }
                }
                else
                    ::usleep(10000);
                continue;
            }
            if(sel==-1){
                GLOGE("network subsystem error: " << errno);
                break;
            }
            if(FD_ISSET(u.sock(),&rfd))
            {
                bytes = u.receive(io, sizeof(io)-1);
                if(bytes<=0)
                {
                    GLOGE("recfrom " << cli << " error:" << errno);
                    break;
                }
                io[bytes]=0;
                if(io[0]==0x13)
                {
                    GLOGE("terminal ended");
                    break;
                }
                _iopump.to_shell(string(io));
            }
            FD_CLR(u.sock(), &rfd);
        }
        u.destroy();
        GLOGT("xterm exits");
    }
    return EX_NONE;
}

