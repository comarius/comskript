#ifndef UDPTTY_H
#define UDPTTY_H

#include <sock.h>
#include "remotetty.h"
#include "sockimpl.h"

class udptty : public remotetty, public sockimpl
{
public:
    udptty();
    ~udptty();

    virtual eNUMEXP  connect(const char* user, const char* pass, const char* host, int port, const string& args);
    virtual eNUMEXP  connect();
    virtual eNUMEXP  open_tty();
    virtual eNUMEXP  receive(string& s);
    virtual eNUMEXP  send(const string& c);
    virtual eNUMEXP  put_file(const char* src, const char* dst);
    virtual eNUMEXP  get_file(const char* src, const char* dst);
    virtual bool     isopen();
    virtual void     close();
private:
    udp_sock    _srv;
    SADDR_46    _remote;
};

#endif // UDPTTY_H
