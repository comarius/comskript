#ifndef FXNGINE_H
#define FXNGINE_H

#include "os.h"
#include <string>
#include <set>
#include <deque>
#include <stack>
#include <vector>
#include "sqwrap.h"
#include "testate.h"
#include "concontext.h"

//-----------------------------------------------------------------------------
class Context;


struct ScriptData
{
    ScriptData(){}
    ScriptData(SqEnv* penv, const string& script,const string& mid, Context* pc):_penv(penv),_script(script),_defunct(pc),_deviceid(mid){}
    SqEnv*      _penv;
    string      _script;
    Context*    _defunct;
    string      _deviceid;
    string      _logpath;
};

//-----------------------------------------------------------------------------
class fxngine : public OsThread
{
public:
    fxngine();
    virtual ~fxngine();
    void    run(int nargs,  char* v[]);
    const   std::string& get_input(Context* pc);
    void    sq_run(const char* cred, const char* mid, SqEnv& env, const string& sf);
    void    run_local(const char* test, const char* mid);
    void    run_remote(const char* test, const char* cred, const char* mid);
    void    save_data(const ScriptData& s){ _sdata = s;}
    const   ScriptData&  get_data(){return _sdata;}
    const   string& credentials()const{return _maincred;}
    SqEnv&  getsq_env(){return _env;};
    const string& ssq_error()const{return _sqerror;}
    bool    wasq_error(){return !_sqerror.empty();}
    void    sq_error(const char* buff){_sqerror += buff;}
    bool    run_inst(const char* s, const char* t, const char* i);
    void    thread_main();
    uint32_t    count_instances();
    static void chekin();
private:
    string  _base_name(const char* test);
    size_t  _load_tests(std::vector<std::string>& tests);
    void    _test_target(const std::string& targetid, const std::string& targetip);
    void    _pick_uptest(const std::string& targetid, const std::string& targetip);

private:
    mutex                   _m;
    std::string             _retval;
    ScriptData              _sdata; //pass info to Ctx because the script creates instance
    string                  _maincred;
    string                  _deviceid;
    SqEnv                   _env;
    string                  _sqerror;
    deque<string>           _procque;
    vector<string>          _children;
    static int              __lastactivity;
};

extern fxngine* PFX;

#endif // FXNGINE_H
