#ifndef USBTTY_H
#define USBTTY_H

#include "remotetty.h"


class usbtty : public remotetty
{
public:
    usbtty();
    virtual ~usbtty();
    virtual eNUMEXP connect(const char* user, const char* pass, const char* host, int port, const string& args);
    virtual eNUMEXP connect();
    virtual eNUMEXP open_tty();
    virtual eNUMEXP receive(string& s);
    virtual eNUMEXP send(const string& c);
    virtual eNUMEXP put_file(const char* src, const char* dst);
    virtual eNUMEXP get_file(const char* src, const char* dst);
    virtual bool     isopen();
    virtual void     close();
protected:
private:
};

#endif // USBTTY_H
